package net.ravenix.core.bukkit.skin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.core.shared.skin.Skin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public final class SkinProvider implements ISkinProvider {

    private final MySQL mySQL;

    private final Map<UUID, ISkin> skinData = Maps.newHashMap();

    public SkinProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadSkinData();
    }

    @Override
    public ISkin getSkin(UUID uuid) {
        return this.skinData.get(uuid);
    }

    @Override
    public void updateSkin(UUID uuid, String skinValue, String skinSignature, boolean send, boolean insert) {
        ISkin skin;
        if (this.skinData.get(uuid) == null) {
            skin = new Skin(uuid, skinValue, skinSignature);
            this.skinData.put(uuid, skin);
        } else {
            skin = getSkin(uuid);
            if (!skin.getSkinSignature().equalsIgnoreCase(skinSignature) || !skin.getSkinValue().equalsIgnoreCase(skinValue)) {
                skin.setSkinSignature(skinSignature);
                skin.setSkinValue(skinValue);
                this.mySQL.update("UPDATE skinData SET skinValue='" + skinValue + "', skinSignature='" + skinSignature + "' WHERE uuid='" + uuid.toString() + "'");
            }
        }
        if (insert) {
            this.mySQL.update("INSERT INTO skinData (uuid,skinValue,skinSignature) VALUES ('" + uuid.toString()
                    + "','" + skinValue + "','" + skinSignature + "')");
        }

        if (send) {
            updateSkinData(uuid, skinValue, skinSignature);
        }
    }

    @Override
    public ISkin getRandomSkin() {
        Collection<ISkin> values = this.skinData.values();
        List<ISkin> skins = Lists.newArrayList();
        skins.addAll(values);
        return skins.get(new Random().nextInt(skins.size() - 1));
    }

    private void updateSkinData(UUID uuid, String skinValue, String skinSignature) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("skinValue", skinValue);
        jsonDocument.append("skinSignature", skinSignature);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("skinUpdate", "update", jsonDocument);
    }

    private void loadSkinData() {
        ResultSet result = this.mySQL.getResult("SELECT * FROM skinData");
        while (true) {
            try {
                assert result != null;
                if (!result.next()) break;

                UUID uuid = UUID.fromString(result.getString("uuid"));
                String skinValue = result.getString("skinValue");
                String skinSignature = result.getString("skinSignature");
                ISkin skin = new Skin(uuid, skinValue, skinSignature);
                this.skinData.put(uuid, skin);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
