package net.ravenix.core.bukkit.command;

import com.google.common.collect.Lists;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.bukkit.tablist.TabList;
import net.ravenix.core.bukkit.tablist.TabListProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public final class NickCommand
        implements CommandExecutor {

    private final List<UUID> cooldown = Lists.newArrayList();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!player.hasPermission("ravenix.command.nick")) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return true;
        }
        if (this.cooldown.contains(player.getUniqueId())) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cBitte warte einen Moment.");
            return true;
        }
        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        this.cooldown.add(player.getUniqueId());
        Bukkit.getScheduler().runTaskLater(BukkitCore.getInstance(), () -> {
            this.cooldown.remove(player.getUniqueId());
        }, 20L * 3);
        if (nickProvider.isNicked(player.getUniqueId())) {
            Bukkit.getOnlinePlayers().forEach(player1 -> player1.hidePlayer(player));
            nickProvider.unnickPlayer(player.getUniqueId(), true);
            this.update(player);
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDein Nickname wurde entfernt.");
            return true;
        }
        Bukkit.getOnlinePlayers().forEach(player1 -> player1.hidePlayer(player));
        nickProvider.nickPlayer(player.getUniqueId(), true);
        this.update(player);
        return false;
    }

    private void update(Player player ) {
        TabListProvider tabListProvider = BukkitCore.getInstance().getTabListProvider();
        Bukkit.getScheduler().runTask(BukkitCore.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(players -> {
            final TabList tabList = tabListProvider.get(players.getUniqueId());
            players.showPlayer(player);
            tabList.update();
        }));
    }
}
