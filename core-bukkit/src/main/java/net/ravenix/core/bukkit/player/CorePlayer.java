package net.ravenix.core.bukkit.player;

import lombok.AllArgsConstructor;
import net.ravenix.core.shared.player.ICorePlayer;

import java.util.UUID;

@AllArgsConstructor
public final class CorePlayer implements ICorePlayer {

    private final UUID uuid;
    private boolean online;
    private String server;
    private  String proxy;

    @Override
    public UUID getUUID() {
        return this.uuid;
    }

    @Override
    public void setOnline(boolean state) {
        this.online = state;
    }

    @Override
    public boolean isOnline() {
        return this.online;
    }

    @Override
    public void setServer(String serverName) {
        this.server = serverName;
    }

    @Override
    public String getServer() {
        return this.server;
    }

    @Override
    public void setProxy(String proxyName) {
        this.proxy = proxyName;
    }


    @Override
    public String getProxy() {
        return this.proxy;
    }
}
