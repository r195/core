package net.ravenix.core.bukkit.permission.bukkit;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermissionInjector implements Listener {

    @Getter
    private final Map<UUID, BukkitPermissible> permissibleMap = new HashMap<>();

    @EventHandler
    public void handleQuit(PlayerQuitEvent event) {
        this.permissibleMap.remove(event.getPlayer().getUniqueId());
    }

    public void injectPermissible(Player player) {
        try {
            Field field;
            Class<?> clazz = this.reflectCraftClazz(".entity.CraftHumanEntity");
            if (clazz != null) {
                field = clazz.getDeclaredField("perm");
            } else {
                field = Class.forName("net.glowstone.entity.GlowHumanEntity").getDeclaredField("permissions");
            }

            BukkitPermissible permissible = new BukkitPermissible(player);
            this.permissibleMap.put(player.getUniqueId(), permissible);

            field.setAccessible(true);
            field.set(player, permissible);
            field.setAccessible(false);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private Class<?> reflectCraftClazz(String suffix) {
        try {
            String version = org.bukkit.Bukkit.getServer().getClass().getPackage()
                    .getName().split("\\.")[3];
            return Class.forName("org.bukkit.craftbukkit." + version + suffix);
        } catch (Exception ex) {
            try {
                return Class.forName("org.bukkit.craftbukkit." + suffix);
            } catch (ClassNotFoundException ignored) {
                return null;
            }
        }
    }

}
