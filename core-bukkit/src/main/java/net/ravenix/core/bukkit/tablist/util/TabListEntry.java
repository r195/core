package net.ravenix.core.bukkit.tablist.util;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.RandomStringUtils;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class TabListEntry {

    private final String entryId = RandomStringUtils.randomAlphanumeric(8);

    private int position;
    private final UUID owner;
    private WrappedGameProfile gameProfile;
    private String name;
    private String displayName;
    private String prefix;
    private String suffix;

    public TabListEntry(UUID owner, WrappedGameProfile gameProfile) {
        this.owner = owner;
        this.gameProfile = gameProfile;
    }

    public TabListEntry setGameProfile(WrappedGameProfile gameProfile) {
        this.gameProfile = gameProfile;
        return this;
    }

    public TabListEntry setPosition(int position) {
        this.position = position;
        return this;
    }

    public TabListEntry setName(String name) {
        this.name = name;
        return this;
    }

    public TabListEntry setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public TabListEntry setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public TabListEntry setSuffix(String suffix) {
        this.suffix = suffix;
        return this;
    }
}
