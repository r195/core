package net.ravenix.core.bukkit.command;

import de.dytanic.cloudnet.CloudNet;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.provider.service.SpecificCloudServiceProvider;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.util.TimeFormat;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by Gamingcode in 22.09.2021.
 */
public final class IDCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!player.hasPermission("ravenix.command.id")) {
            player.sendMessage("Unknown command. Type \"/help\" for help.");
            return true;
        }
        String serverName = CloudNetDriver.getInstance().getComponentName();

        ServiceInfoSnapshot serviceInfoSnapshot = Objects.requireNonNull(getServiceInfoSnapshot(serverName));
        long creationTime = Objects.requireNonNull(CloudNetDriver.getInstance()
                .getCloudServiceProvider(serviceInfoSnapshot)
                .getServiceInfoSnapshot()).getCreationTime();

        String taskServiceId = serviceInfoSnapshot.getServiceId().getUniqueId().toString();


        long freeMemory = Runtime.getRuntime().freeMemory() / 1000000 - 24;
        long maxMemory = Runtime.getRuntime().maxMemory() / 1000000 - 24;
        long usedMemory = maxMemory - freeMemory;

        player.sendMessage("");
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Server§8: §b" + serverName);
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Erstellt am§8: §b" + TimeFormat.format(creationTime));
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Server-UniqueID§8: §b" + taskServiceId);
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Ram§8: §b" + usedMemory + "MB§7/§b" + maxMemory + "MB");
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Freier Ram§8: §b" + freeMemory + "MB");
        player.sendMessage("");

        return false;
    }

    private ServiceInfoSnapshot getServiceInfoSnapshot(String serverName) {
        for (ServiceInfoSnapshot serviceInfoSnapshot : CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServicesByGroup(serverName.split("-")[0])) {
            if (serviceInfoSnapshot.getName().equalsIgnoreCase(serverName)) {
                return serviceInfoSnapshot;
            }
        }
        return null;
    }
}
