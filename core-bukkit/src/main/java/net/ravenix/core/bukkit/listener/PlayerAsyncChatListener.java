package net.ravenix.core.bukkit.listener;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.event.AsyncPlayerChatForPlayerEvent;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.UUID;

public final class PlayerAsyncChatListener
        implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void handleChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if (event.getMessage().contains("%"))
            event.setMessage(event.getMessage().replace("%", "%%"));


        event.setFormat("%s%s§8: §7" + event.getMessage());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void handleChatFormat(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) return;
        event.setCancelled(true);
        final String format = event.getFormat();

        final Player player = event.getPlayer();

        event.getRecipients().forEach(receiver -> {
            String messageToSend = "§7" + player.getName() + "§8: §7" + event.getMessage();

            final AsyncPlayerChatForPlayerEvent asyncPlayerChatForPlayerEvent = new AsyncPlayerChatForPlayerEvent(player, receiver, format, messageToSend);
            Bukkit.getPluginManager().callEvent(asyncPlayerChatForPlayerEvent);
            if (asyncPlayerChatForPlayerEvent.isCancelled()) return;

            String newFormat = asyncPlayerChatForPlayerEvent.getMessageToSend();
            receiver.sendMessage(newFormat);
        });
    }
    @EventHandler
    public void handleCustomChatEvent(AsyncPlayerChatForPlayerEvent event) {
        final Player player = event.getPlayer();
        final Player viewer = event.getViewer();
        final String format = event.getFormat();

        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();

        final UUID uuid = player.getUniqueId();
        final NickData nickData = nickProvider.getNickData(uuid);

        final boolean canSee = viewer.hasPermission("ravenix.nick.see");

        IPermissionGroup permissionGroup;

        final IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
        if (canSee || nickData == null) {
            permissionGroup = permissionUser.getHighestGroup();
        } else {
            permissionGroup = permissionProvider.getDefaultGroup();
        }

        String prefix = permissionGroup.getPrefix(PrefixType.CHAT);
        String playerName = (canSee || nickData == null) ? player.getName() : nickData.getNickName();
        final String playerMessage = format.replaceFirst(player.getName(), player.getName());

        event.setMessageToSend(String.format(playerMessage, prefix, playerName));
    }
}
