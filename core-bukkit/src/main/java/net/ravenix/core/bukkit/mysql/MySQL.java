package net.ravenix.core.bukkit.mysql;

import net.ravenix.core.bukkit.BukkitCore;
import org.bukkit.Bukkit;

import java.sql.*;

public final class MySQL {

    public static Connection connection;

    public void connect() {
        try {
            connection = DriverManager
                    .getConnection("jdbc:mysql://116.202.58.73:3306/core?autoReconnect=true", "root", "FMzMbOxRzBJy");
            this.mySQLThread();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void mySQLThread() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitCore.getInstance(), () -> {
            if (connection == null) {
                this.connect();
            } else {
                this.getResult("SELECT * FROM core_users");
            }
        }, 0L, 20 * 60 * 20);
    }

    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public void update(String qry) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            disconnect();
            connect();
        }
    }

    public ResultSet getResult(String qry) {
        try {
            return connection.createStatement().executeQuery(qry);
        } catch (SQLException e) {

            e.printStackTrace();

            return null;
        }
    }
}




