package net.ravenix.core.bukkit.event;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@Setter
public class AsyncPlayerChatForPlayerEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Player player;
    private final Player viewer;
    private final String format;

    private boolean cancelled = false;

    private String messageToSend;

    public AsyncPlayerChatForPlayerEvent(Player player, Player viewer, String format, String messageToSend) {
        super(true);
        this.player = player;
        this.viewer = viewer;
        this.format = format;
        this.messageToSend = messageToSend;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
