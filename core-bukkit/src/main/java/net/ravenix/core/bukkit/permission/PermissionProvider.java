package net.ravenix.core.bukkit.permission;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.core.shared.permission.ExpirationData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.group.PermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.permission.user.PermissionUser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;

public final class PermissionProvider implements IPermissionProvider {

    private final List<IPermissionGroup> permissionGroups = Lists.newArrayList();
    private final Map<UUID, IPermissionUser> permissionUserUUIDMap = Maps.newHashMap();

    private final MySQL mySQL;

    public PermissionProvider(MySQL mySQL) {
        this.mySQL = mySQL;
        loadAllGroups();
        loadAllUsers();
    }

    @Override
    public IPermissionGroup getPermissionGroup(String groupName) {
        return this.searchForPermissionGroup(permissionGroup -> permissionGroup.getName().equalsIgnoreCase(groupName));
    }

    @Override
    public IPermissionUser getPermissionUser(UUID uuid) {
        return this.permissionUserUUIDMap.get(uuid);
    }

    @Override
    public List<IPermissionGroup> getPermissionGroups() {
        return this.permissionGroups;
    }

    @Override
    public void addPermissionGroup(IPermissionUser permissionUser, IPermissionGroup permissionGroup, UUID executor, long duration, boolean insert) {
        ExpirationData expirationData = new ExpirationData(permissionGroup, duration);
        permissionUser.addPermissionGroup(permissionGroup, expirationData);

        if (insert) {
            this.mySQL.update("INSERT INTO core_groups_primary (uuid,groupName) VALUES ('"
                    + permissionUser.getUUID().toString() + "','" + permissionGroup.getName() + "')");
            this.insertExpiration(permissionUser, permissionGroup, duration);
            updatePermissions(permissionUser, true, permissionGroup, expirationData);
        }
    }

    private void deleteExpiration(IPermissionUser permissionUser, IPermissionGroup permissionGroup) {
        this.mySQL.update("DELETE FROM `core_users_expirations` WHERE uuid ='"
                + permissionUser.getUUID().toString() +"' AND groupName = '" + permissionGroup.getName() + "';");
    }

    private void insertExpiration(IPermissionUser permissionUser, IPermissionGroup permissionGroup, long duration) {
        this.mySQL.update("INSERT INTO core_users_expirations (uuid,groupName,expiration) VALUES ('"
                + permissionUser.getUUID().toString() + "','" + permissionGroup.getName() + "','" + duration +"')");
    }

    @Override
    public void removePermissionGroup(IPermissionUser permissionUser, UUID executor, IPermissionGroup permissionGroup) {
        permissionUser.removePermissionGroup(permissionGroup);

        this.mySQL.update("DELETE FROM `core_groups_primary` WHERE uuid ='"
                + permissionUser.getUUID().toString() +"' AND groupName = '" + permissionGroup.getName() + "';");
        this.deleteExpiration(permissionUser, permissionGroup);

        if (permissionUser.getHighestGroup() == null) {
            IPermissionGroup defaultGroup = this.getDefaultGroup();
            this.addPermissionGroup(permissionUser, null, defaultGroup, true);
            permissionUser.addPermissionGroup(defaultGroup, new ExpirationData(defaultGroup, -1));
        }
        updatePermissions(permissionUser, false, permissionGroup, null);
    }

    private void updatePermissions(IPermissionUser permissionUser, boolean add, IPermissionGroup permissionGroup, ExpirationData expirationData) {
        permissionUser.calculatePermissions();
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", permissionUser.getUUID().toString());
        jsonDocument.append("add", String.valueOf(add));
        jsonDocument.append("groupName", permissionGroup.getName());
        if (expirationData != null) {
            jsonDocument.append("expiration", expirationData.getExpiration());
        }

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("permissionUpdate", "update", jsonDocument);
    }

    @Override
    public IPermissionGroup getDefaultGroup() {
        return this.searchForPermissionGroup(IPermissionGroup::isDefault);
    }

    @Override
    public void createPermissionUser(UUID uuid, boolean insert) {
      if (getPermissionUser(uuid) != null) return;

      PermissionUser permissionUser = new PermissionUser(uuid);
      permissionUser.addPermissionGroup(this.getDefaultGroup(), new ExpirationData(this.getDefaultGroup(), -1));

      this.permissionUserUUIDMap.put(uuid, permissionUser);
    }

    @Override
    public List<IPermissionUser> getPermissionUsers() {
        List<IPermissionUser> permissionUserList = Lists.newArrayList();
        this.permissionUserUUIDMap.forEach((uuid, iPermissionUser) -> {
            permissionUserList.add(iPermissionUser);
        });
        return permissionUserList;
    }

    private IPermissionGroup searchForPermissionGroup(Predicate<IPermissionGroup> predicate) {
        return this.permissionGroups.stream()
                .filter(predicate)
                .findFirst().orElse(null);
    }

    private void loadAllGroups() {
        List<PermissionGroup> permissionGroupCache = Lists.newArrayList();
        ResultSet groupResult = this.mySQL.getResult("SELECT * FROM core_groups order by id desc;");
        while (true) {
            try {
                assert groupResult != null;
                if (!groupResult.next()) break;
                PermissionGroup permissionGroup = new PermissionGroup(groupResult.getString("name"),
                        groupResult.getInt("priority"),
                        groupResult.getBoolean("defaultGroup"),
                        groupResult.getString("color"));

                String tabPrefix = groupResult.getString("tabPrefix");
                String nameTagPrefix = groupResult.getString("nameTagPrefix");
                String chatPrefix = groupResult.getString("chatPrefix");
                String displayPrefix = groupResult.getString("displayPrefix");

                permissionGroup.getPrefixes().put(PrefixType.TAB, tabPrefix);
                permissionGroup.getPrefixes().put(PrefixType.DISPLAY, displayPrefix);
                permissionGroup.getPrefixes().put(PrefixType.NAMETAG, nameTagPrefix);
                permissionGroup.getPrefixes().put(PrefixType.CHAT, chatPrefix);

                permissionGroupCache.add(permissionGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        permissionGroupCache.forEach(permissionGroup -> {
            ResultSet inheritsResult
                    = this.mySQL.getResult("SELECT * FROM core_groups_inheritances WHERE groupName='" + permissionGroup.getName() + "'");
            while (true) {
                try {
                    assert inheritsResult != null;
                    if (!inheritsResult.next()) break;
                    final IPermissionGroup inheritedGroup = getInheritedGroup(permissionGroups, inheritsResult.getString("inherits"));

                    if (inheritedGroup != null) permissionGroup.addInheritance(inheritedGroup);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            ResultSet permissionResult
                    = this.mySQL.getResult("SELECT * FROM core_groups_permissions WHERE groupName='" + permissionGroup.getName() + "'");
            while (true) {
                try {
                    assert permissionResult != null;
                    if (!permissionResult.next()) break;

                    final String permission = permissionResult.getString("permission");
                    permissionGroup.addPermission(permission, permissionResult.getBoolean("negate"));

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            this.permissionGroups.add(permissionGroup);
        });
    }

    private IPermissionGroup getInheritedGroup(List<IPermissionGroup> groups, String name) {
        return groups.stream().filter(iPermissionGroup -> iPermissionGroup.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    private void loadAllUsers() {
        List<PermissionUser> permissionUsersCache = Lists.newArrayList();
        ResultSet userResult = this.mySQL.getResult("SELECT * FROM core_users");
        while (true) {
            try {
                assert userResult != null;
                if (!userResult.next()) break;

                UUID uuid = UUID.fromString(userResult.getString("uuid"));
                PermissionUser permissionUser = new PermissionUser(uuid);
                permissionUsersCache.add(permissionUser);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        permissionUsersCache.forEach(permissionUser -> {
            ResultSet primaryResult = this.mySQL.getResult("SELECT * FROM core_groups_primary WHERE uuid='" + permissionUser.getUUID().toString() + "'");

            while (true) {
                try {
                    assert primaryResult != null;
                    if (!primaryResult.next()) break;

                    String primaryGroupName = primaryResult.getString("groupName");

                    IPermissionGroup primaryGroup = getPermissionGroup(primaryGroupName);

                    if (primaryGroup != null) {
                        permissionUser.addPermissionGroup(primaryGroup, new ExpirationData(primaryGroup, -1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            ResultSet expirations = this.mySQL.getResult("SELECT * FROM core_users_expirations WHERE uuid='" + permissionUser.getUUID().toString() + "'");

            while (true) {
                try {
                    assert expirations != null;
                    if (!expirations.next()) break;

                    String groupName = expirations.getString("groupName");
                    IPermissionGroup permissionGroup = getPermissionGroup(groupName);
                    ExpirationData expirationData = new ExpirationData(permissionGroup, expirations.getLong("expiration"));

                    if (permissionUser.getGroupExpirations().stream().noneMatch(expirationData1 -> expirationData1.getExpiration() == expirationData1.getExpiration()
                            && expirationData1.getPermissionGroup().equals(expirationData.getPermissionGroup())))
                        permissionUser.getGroupExpirations().add(expirationData);

                    this.permissionUserUUIDMap.put(permissionUser.getUUID(), permissionUser);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
