package net.ravenix.core.bukkit.tablist;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardTeam;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.*;
import com.google.common.collect.Lists;
import lombok.Getter;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.event.TabListUpdateForPlayerEvent;
import net.ravenix.core.bukkit.tablist.util.TabListEntry;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.skin.Skin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class TabList {

    private final ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

    private final List<TabListEntry> entries = new CopyOnWriteArrayList<>();

    private final UUID owner;

    public TabList(UUID uuid) {
        this.owner = uuid;
    }

    public void addEntry(TabListEntry tabListEntry) {
        this.entries.add(tabListEntry);
    }

    public void removeEntry(TabListEntry tabListEntry) {
        this.deleteTeam(tabListEntry);
        this.entries.remove(tabListEntry);
    }

    public void removeEntry(UUID uuid) {
        final TabListEntry entry = this.getEntry(uuid);
        this.removeEntry(entry);
    }

    public void createTeam(TabListEntry entry) {
        final Player player = Bukkit.getPlayer(entry.getOwner());
        if (player == null) return;

        final String prefix = entry.getPrefix();
        final String suffix = entry.getSuffix();

        final WrapperPlayServerScoreboardTeam wrapperPlayServerScoreboardTeam = new WrapperPlayServerScoreboardTeam();
        wrapperPlayServerScoreboardTeam.setMode(WrapperPlayServerScoreboardTeam.Mode.TEAM_CREATED);

        wrapperPlayServerScoreboardTeam.setPrefix(this.formatColors(this.cutString(prefix, 16)));
        wrapperPlayServerScoreboardTeam.setSuffix(this.formatColors(this.cutString(suffix, 16)));

        wrapperPlayServerScoreboardTeam.setDisplayName(this.cutString(entry.getDisplayName(), 32));
        wrapperPlayServerScoreboardTeam.setPlayers(Lists.newArrayList(entry.getGameProfile().getName()));
        wrapperPlayServerScoreboardTeam.setNameTagVisibility("ALWAYS");
        final String entryName = this.getEntryName(entry);

        wrapperPlayServerScoreboardTeam.setName(entryName);
        this.sendPacket(wrapperPlayServerScoreboardTeam.getHandle());
    }

    public void deleteTeam(TabListEntry entry) {
        WrapperPlayServerScoreboardTeam team = new WrapperPlayServerScoreboardTeam();
        team.setMode(1);
        team.setName(getEntryName(entry));
        this.sendPacket(team.getHandle());
    }

    public void updateTeam(TabListEntry entry) {
        final Player player = Bukkit.getPlayer(entry.getOwner());
        if (player == null) return;

        final String entryName = this.getEntryName(entry);

        final String prefix = entry.getPrefix();
        final String suffix = entry.getSuffix();

        final WrapperPlayServerScoreboardTeam wrapperPlayServerScoreboardTeam = new WrapperPlayServerScoreboardTeam();
        wrapperPlayServerScoreboardTeam.setMode(WrapperPlayServerScoreboardTeam.Mode.TEAM_UPDATED);
        wrapperPlayServerScoreboardTeam.setName(entryName);
        wrapperPlayServerScoreboardTeam.setDisplayName(this.cutString(entry.getDisplayName(), 16));

        wrapperPlayServerScoreboardTeam.setPrefix(prefix);
        wrapperPlayServerScoreboardTeam.setSuffix(suffix);

        this.sendPacket(wrapperPlayServerScoreboardTeam.getHandle());
    }

    public void updateDisplayName(TabListEntry entry) {
        final WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
        wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.UPDATE_DISPLAY_NAME);

        if (entry.getDisplayName().equalsIgnoreCase(entry.getName())) return;

        final PlayerInfoData playerInfoData = new PlayerInfoData(
                entry.getGameProfile(),
                0,
                EnumWrappers.NativeGameMode.NOT_SET,
                WrappedChatComponent.fromText(entry.getDisplayName())
        );

        wrapperPlayServerPlayerInfo.setData(Lists.newArrayList(playerInfoData));
        this.sendPacket(wrapperPlayServerPlayerInfo.getHandle());
    }

    public void setSkinData(TabListEntry tabListEntry, Skin skin) {
        final WrappedGameProfile gameProfile = tabListEntry.getGameProfile();
        gameProfile.getProperties().put("textures",
                new WrappedSignedProperty("textures",
                        skin.getSkinValue(),
                        skin.getSkinSignature()));
    }

    public void update() {
        Bukkit.getScheduler().runTaskAsynchronously(BukkitCore.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(player -> {
            final TabListUpdateForPlayerEvent tabListUpdateForPlayerEvent = new TabListUpdateForPlayerEvent(player, Bukkit.getPlayer(this.owner), this);
            Bukkit.getPluginManager().callEvent(tabListUpdateForPlayerEvent);
            if (tabListUpdateForPlayerEvent.isCancelled()) return;

            TabListEntry entry = getEntry(player.getUniqueId());
            if (entry != null) {
                this.deleteTeam(entry);
            }

            final String name = tabListUpdateForPlayerEvent.getCustomName() != null ? tabListUpdateForPlayerEvent.getCustomName() : player.getName();
            entry = new TabListEntry(tabListUpdateForPlayerEvent.getPosition(),
                    player.getUniqueId(),
                    new WrappedGameProfile(tabListUpdateForPlayerEvent.getCustomUniqueId() != null ? tabListUpdateForPlayerEvent.getCustomUniqueId() : player.getUniqueId(),
                            name),
                    name,
                    this.constructDisplayName(name, tabListUpdateForPlayerEvent),
                    this.getPrefix(tabListUpdateForPlayerEvent),
                    this.getSuffix(tabListUpdateForPlayerEvent));

            this.addEntry(entry);
            this.sendTeamPacket(entry);

            TabListEntry finalEntry = entry;
            Bukkit.getScheduler().runTaskLater(BukkitCore.getInstance(), () -> this.updateDisplayName(finalEntry), 2);
        }));
    }

    public void reSet() {
        for (TabListEntry entry : this.entries) {
            this.deleteTeam(entry);
        }

        this.update();
    }

    public TabListEntry getEntry(UUID uuid) {
        return this.entries.stream().filter(entry -> entry.getOwner().equals(uuid)).findFirst().orElse(null);
    }

    public void unregister() {
        this.getEntries().forEach(this::removeEntry);
    }

    private String getEntryName(TabListEntry entry) {
        String shortName = this.cutString(entry.getGameProfile().getName(), 5);
        char codePoint = (char) ((char) entry.getPosition() + 20);
        return cutString(codePoint + shortName + entry.getEntryId(), 16);
    }

    private String cutString(String s, int maxLength) {
        if (s.length() > maxLength)
            return s.substring(0, maxLength);
        return s;
    }

    private String joiningMap(final Map<String, String> in) {
        if (in == null) return "";
        final StringBuilder stringBuilder = new StringBuilder();
        in.forEach((s, s2) -> stringBuilder.append(s2));
        return stringBuilder.toString();
    }

    private String constructDisplayName(String name, TabListUpdateForPlayerEvent event) {
        final String prefix = this.joiningMap(event.getPrefixes().get(PrefixType.TAB));
        final String suffix = this.joiningMap(event.getSuffixes().get(PrefixType.TAB));

        return this.formatColors(prefix + name + suffix);
    }

    private String getPrefix(TabListUpdateForPlayerEvent event) {
        return this.cutString(this.formatColors(this.joiningMap(event.getPrefixes().get(PrefixType.NAMETAG))), 16);
    }

    private String getSuffix(TabListUpdateForPlayerEvent event) {
        return this.cutString(this.formatColors(this.joiningMap(event.getSuffixes().get(PrefixType.NAMETAG))), 16);
    }

    private String formatColors(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    private void sendTeamPacket(TabListEntry entry) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.SCOREBOARD_TEAM);

        packet.getStrings().write(0, getEntryName(entry));
        packet.getIntegers().write(1, 0);
        packet.getStrings().write(1, this.cutString(entry.getDisplayName(), 32));
        packet.getStrings().write(2, this.cutString(entry.getPrefix(), 16));
        packet.getStrings().write(3, this.cutString(entry.getSuffix(), 16));
        packet.getStrings().write(4, "always");
        packet.getSpecificModifier(Collection.class).write(0, Lists.newArrayList(entry.getGameProfile().getName()));
        this.sendPacket(packet);
    }

    private void sendPacket(PacketContainer container) {
        Player player = Bukkit.getPlayer(this.owner);
        if (player == null || !player.isOnline()) return;
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}