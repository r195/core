package net.ravenix.core.bukkit.nickname.packet;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.skin.ISkin;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerInfoDataPacketAdapter extends PacketAdapter {

    public PlayerInfoDataPacketAdapter() {
        super(BukkitCore.getInstance(), ListenerPriority.HIGH, PacketType.Play.Server.PLAYER_INFO);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        final Player player = event.getPlayer();

        final WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo(event.getPacket());

        final List<PlayerInfoData> newData = new ArrayList<>();

        for (PlayerInfoData playerInfoData : wrapperPlayServerPlayerInfo.getData()) {
            final UUID uuid = playerInfoData.getProfile().getUUID();
            final String realName = playerInfoData.getProfile().getName();

            if (uuid.equals(player.getUniqueId()) || uuid.getLeastSignificantBits() == 0 || playerInfoData.getLatency() == -88) {
                newData.add(playerInfoData);
                continue;
            }

            NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();

            final NickData nickData = nickProvider.getNickData(uuid);

            if (nickData != null) {
                if (player.hasPermission("ravenix.nick.see")) {
                    newData.add(playerInfoData);
                    continue;
                }

                final WrappedGameProfile wrappedGameProfile = new WrappedGameProfile(nickData.getNickedUUID(), nickData.getNickName());
                final WrappedChatComponent displayName = playerInfoData.getDisplayName();

                final ISkin customSkin = nickData.getNickedSkin();
                if (customSkin != null) {
                    wrappedGameProfile.getProperties().put("textures",
                            new WrappedSignedProperty("textures",
                                    customSkin.getSkinValue(),
                                    customSkin.getSkinSignature()));
                }

                if (displayName != null) {
                    displayName.setJson(displayName.getJson().replaceAll(realName, nickData.getNickName()));
                }

                final PlayerInfoData infoData = new PlayerInfoData(wrappedGameProfile,
                        playerInfoData.getLatency(),
                        playerInfoData.getGameMode(),
                        displayName);

                newData.add(infoData);
            } else {
                newData.add(playerInfoData);
            }
        }

        wrapperPlayServerPlayerInfo.setData(newData);

        event.setPacket(wrapperPlayServerPlayerInfo.getHandle());
    }
}
