package net.ravenix.core.bukkit.permission.bukkit;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.permission.user.PermissionUser;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;

import java.util.UUID;

public class BukkitPermissible extends PermissibleBase {

    private IPermissionUser permissionUser;

    public BukkitPermissible(Player player) {
        super(player);

        this.permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(player.getUniqueId());
        this.permissionUser.calculatePermissions();
    }

    @Override
    public boolean hasPermission(String inName) {
        return this.permissionUser.hasPermission(inName) || this.permissionUser.hasPermission(inName);
    }

    @Override
    public boolean hasPermission(Permission perm) {
        return this.hasPermission(perm.getName());
    }

    public void recalculatePermissions(UUID uuid) {
        this.permissionUser = BukkitCore.getInstance().getPermissionProvider().getPermissionUser(uuid);
        if (this.permissionUser != null)
            this.permissionUser.calculatePermissions();
    }
}

