package net.ravenix.core.bukkit.inventory;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.util.ItemBuilder;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

public class InventoryUtil {

    private static void updateSpectatorForReport(UUID suspect, UUID spectator) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("suspect", suspect.toString());
        jsonDocument.append("spectator", spectator.toString());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateSpectator", "update", jsonDocument);
    }

    public static void openEditInventory(Player player, String suspectName) {
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        NameResult nameResultByName = nameStorageProvider.getNameResultByName(suspectName);
        IReportProvider reportProvider = BukkitCore.getInstance().getReportProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResultByName.getUuid());
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "§bReports §8» " + permissionUser.getHighestGroup().getColor() + suspectName);

        IReport report = reportProvider.getReport(nameResultByName.getUuid());
        if (report.getSpectator() == null) {
            report.setSpectator(player.getUniqueId());
            updateSpectatorForReport(nameResultByName.getUuid(), player.getUniqueId());
        }

        inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());

        if (report.getSpectator() != null) {
            if (!report.getSpectator().equals(player.getUniqueId())) {
                NameResult nameResult = nameStorageProvider.getNameResultByUUID(report.getSpectator());
                IPermissionUser spectator = permissionProvider.getPermissionUser(nameResult.getUuid());
                String name = spectator.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName();
                player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDieser Report wird bereits von " + name + " §cbearbeitet.");
                player.closeInventory();
                return;
            }
            ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
            ISkin skin = skinProvider.getSkin(nameResultByName.getUuid());
            inventory.setItem(10, new ItemBuilder(Material.SKULL_ITEM, 1, 3).headValue(skin.getSkinValue())
                    .name("§bInfos zu " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByName.getName()).itemStack());

            inventory.setItem(12, new ItemBuilder(Material.ENDER_PEARL, 1).name("§bSpringe zum Spieler").itemStack());

            inventory.setItem(15, new ItemBuilder(Material.INK_SACK, 1, 11).name("§eUnbearbeitet lassen").itemStack());
            inventory.setItem(16, new ItemBuilder(Material.INK_SACK, 1, 1).name("§cReport ablehnen").itemStack());
        }

        inventory.setItem(18, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(19, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(20, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(21, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(22, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(23, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(24, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(25, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());
        inventory.setItem(26, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 7).name("§0").itemStack());

        player.openInventory(inventory);
    }

    public static void openOpenReportsInventory(Player player) {
        IReportProvider reportProvider = BukkitCore.getInstance().getReportProvider();
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, "§bReports");
        if (reportProvider.getOpenReports().size() == 0) {
            inventory.setItem(13, new ItemBuilder(Material.BARRIER).name("§cKeine Reports offen").itemStack());
        } else {
            IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
            INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
            ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
            reportProvider.getOpenReports().forEach(iReport -> {
                IPermissionUser permissionUser = permissionProvider.getPermissionUser(iReport.getSuspect());
                NameResult suspect = nameStorageProvider.getNameResultByUUID(iReport.getSuspect());
                IPermissionUser reporterPermUser = permissionProvider.getPermissionUser(iReport.getReporter());
                NameResult reporter = nameStorageProvider.getNameResultByUUID(iReport.getReporter());
                IPermissionUser specPermUser = permissionProvider.getPermissionUser(iReport.getSpectator());
                NameResult spectator = nameStorageProvider.getNameResultByUUID(iReport.getSpectator());

                if (iReport.getSpectator() == null) {
                    inventory.addItem(new ItemBuilder(Material.SKULL_ITEM, 1, 3)
                            .name(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + suspect.getName())
                            .headValue(skinProvider.getSkin(iReport.getSuspect()).getSkinValue())
                            .lore("§7Grund§8: §b" + iReport.getReason())
                            .lore("§7Server§8: §b" + iReport.getServer())
                            .lore("§7Gemeldet von§8: §b" + reporterPermUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + reporter.getName())
                            .lore("§7Spectator§8: §cNiemand")
                            .itemStack());
                } else {
                    inventory.addItem(new ItemBuilder(Material.SKULL_ITEM, 1, 3)
                            .name(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + suspect.getName())
                            .headValue(skinProvider.getSkin(iReport.getSuspect()).getSkinValue())
                            .lore("§7Grund§8: §b" + iReport.getReason())
                            .lore("§7Server§8: §b" + iReport.getServer())
                            .lore("§7Gemeldet von§8: §b" + reporterPermUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + reporter.getName())
                            .lore("§7Spectator§8: §b" + specPermUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + spectator.getName())
                            .itemStack());
                }
            });
        }

        player.openInventory(inventory);
    }

}
