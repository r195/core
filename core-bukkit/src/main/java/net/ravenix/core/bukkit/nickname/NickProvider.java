package net.ravenix.core.bukkit.nickname;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.nickname.INickProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public final class NickProvider implements INickProvider {

    @Getter
    private final Map<UUID, NickData> cachedNickDatas = Maps.newHashMap();

    @Getter
    private final List<String> nickNames = Lists.newArrayList();

    @Getter
    private final List<String> usedNickNames = Lists.newArrayList();

    private final MySQL mySQL;

    public NickProvider(MySQL mySQL) {
        this.mySQL = mySQL;
        this.mySQL.update("CREATE TABLE IF NOT EXISTS nickNames (nickName varchar(16))");
        loadNickNames();
    }

    @Override
    public NickData getNickData(UUID uuid) {
        return this.cachedNickDatas.get(uuid);
    }

    @Override
    public void nickPlayer(UUID uuid, boolean send) {
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
        String playerName = nameResult.getName();
        String nickName = getRandomNickName();
        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        ISkin randomSkin = skinProvider.getRandomSkin();
        if (send) {
            prepareNicKData(uuid, playerName, nickName, randomSkin.getUUID(), true);
            return;
        }
        prepareNicKData(uuid, playerName, nickName, randomSkin.getUUID(), false);
    }

    @Override
    public void unnickPlayer(UUID uuid, boolean send) {
        removeNickData(uuid, send);
    }

    @Override
    public void prepareNicKData(UUID uuid, String playerName, String nickName, UUID nickedSkinUUID, boolean send) {
        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        ISkin skin = skinProvider.getSkin(nickedSkinUUID);
        NickData nickData = new NickData(playerName, nickName, UUID.randomUUID(), skin);
        this.cachedNickDatas.putIfAbsent(uuid, nickData);
        if (!this.usedNickNames.contains(nickData.getNickName()))
            this.usedNickNames.add(nickData.getNickName());
        if (send) {
            sendNickData(uuid, nickData);
        }

    }

    @Override
    public void removeNickData(UUID uuid, boolean send) {
        NickData nickData = getNickData(uuid);
        if (nickData == null) return;
        this.usedNickNames.remove(nickData.getNickName());
        this.cachedNickDatas.remove(uuid);

        if (send) {
            removeNickData(uuid);
        }
    }

    @Override
    public List<String> getNickNames() {
        return this.nickNames;
    }

    @Override
    public String getRandomNickName() {
        String nickName = this.nickNames.get(new Random().nextInt(this.nickNames.size() - 1));
        if (this.usedNickNames.contains(nickName)) {
            return getRandomNickName();
        }
        return nickName;
    }

    @Override
    public void addNickName(String nickName, boolean send) {
        if (!this.nickNames.contains(nickName))
            this.nickNames.add(nickName);

        if (send) {
            this.mySQL.update("INSERT INTO nickNames (nickName) VALUES ('" + nickName + "')");
            sendNickName(nickName, false);
        }

    }

    private void sendNickName(String nickName, boolean removed) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("nickName", nickName);
        jsonDocument.append("removed", removed);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("sendNickname", "update", jsonDocument);
    }

    @Override
    public void removeNickName(String nickName, boolean send) {
        this.nickNames.remove(nickName);

        if (send) {
            this.mySQL.update("DELETE FROM nickNames WHERE nickName='" + nickName + "'");
            sendNickName(nickName, true);
        }
    }

    @Override
    public boolean existsNickName(String nickName) {
        return this.nickNames.contains(nickName);
    }

    @Override
    public boolean isNicked(UUID uuid) {
        return getNickData(uuid) != null;
    }

    @Override
    public void sendNickDatas() {
        this.cachedNickDatas.forEach((uuid, nickData) -> {
            JsonDocument jsonDocument = new JsonDocument();
            jsonDocument.append("uuid", uuid.toString());
            jsonDocument.append("nickName", nickData.getNickName());
            jsonDocument.append("nickedSkinUUID", nickData.getNickedSkin().getUUID().toString());
            jsonDocument.append("playerName", nickData.getPlayerName());
            CloudNetDriver.getInstance().getMessenger().sendChannelMessage("sendNickDatas", "update", jsonDocument);
        });
    }

    private void removeNickData(UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("removeNickData", "update", jsonDocument);
    }

    private void sendNickData(UUID uuid, NickData nickData) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("playerName", nickData.getPlayerName());
        jsonDocument.append("nickName", nickData.getNickName());
        jsonDocument.append("nickedSkinUUID", nickData.getNickedSkin().getUUID().toString());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("sendNickData", "update", jsonDocument);
    }

    private void loadNickNames() {
        ResultSet result = this.mySQL.getResult("SELECT * FROM nickNames");
        while (true) {
            try {
                assert result != null;
                if (!result.next()) break;
                String nickName = result.getString("nickName");
                this.nickNames.add(nickName);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
