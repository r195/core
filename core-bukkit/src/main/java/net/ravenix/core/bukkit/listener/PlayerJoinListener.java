package net.ravenix.core.bukkit.listener;

import com.mojang.authlib.properties.Property;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.bukkit.permission.bukkit.BukkitPermissible;
import net.ravenix.core.bukkit.permission.bukkit.PermissionInjector;
import net.ravenix.core.bukkit.tablist.TabListProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Collection;

public final class PlayerJoinListener
        implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        TabListProvider tabListProvider = BukkitCore.getInstance().getTabListProvider();

        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        NickData nickData = nickProvider.getNickData(player.getUniqueId());
        if (nickData != null) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDein Nickname lautet§8: §7" + nickData.getNickName());
        }

        CraftPlayer craftPlayer = (CraftPlayer) player;
        Collection<Property> textures = craftPlayer.getProfile().getProperties().get("textures");
        String value = textures.stream().findFirst().get().getValue();
        String signature = textures.stream().findFirst().get().getSignature();

        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        ISkin skin = skinProvider.getSkin(player.getUniqueId());
        skinProvider.updateSkin(player.getUniqueId(), value, signature, true, skin == null);

        PermissionInjector permissionInjector = BukkitCore.getInstance().getPermissionInjector();
        permissionInjector.injectPermissible(player);

        tabListProvider.createForPlayer(player);
        tabListProvider.updateAll();

        Bukkit.getScheduler().runTaskLater(BukkitCore.getInstance(), () -> {
            BukkitPermissible bukkitPermissible = permissionInjector.getPermissibleMap().get(player.getUniqueId());
            bukkitPermissible.recalculatePermissions(player.getUniqueId());
        }, 20L);
    }
}
