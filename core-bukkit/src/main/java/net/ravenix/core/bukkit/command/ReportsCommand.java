package net.ravenix.core.bukkit.command;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.inventory.InventoryUtil;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.report.IReportProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class ReportsCommand
        implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!player.hasPermission("ravenix.command.reports")) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return true;
        }
        InventoryUtil.openOpenReportsInventory(player);
        IReportProvider reportProvider = BukkitCore.getInstance().getReportProvider();
        reportProvider.getReports().forEach(iReport -> {
            if (iReport.getSpectator() != null) {
                if (iReport.getSpectator().equals(player.getUniqueId())) {
                    NameResult nameResultByUUID = BukkitCore.getInstance().getNameStorageProvider().getNameResultByUUID(iReport.getSuspect());
                    InventoryUtil.openEditInventory(player, nameResultByUUID.getName());
                }
            }
        });
        return false;
    }
}
