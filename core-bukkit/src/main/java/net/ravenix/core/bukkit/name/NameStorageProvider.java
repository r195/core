package net.ravenix.core.bukkit.name;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class NameStorageProvider implements INameStorageProvider {

    private final Map<UUID, NameResult> nameResultMap = Maps.newHashMap();
    private final Map<String, NameResult> nameResultByNameMap = Maps.newHashMap();

    private final MySQL mySQL;

    public NameStorageProvider(MySQL mySQL) {
        this.mySQL = mySQL;
        loadNameStorages();
    }

    @Override
    public List<NameResult> getAllNameStorages() {
        List<NameResult> nameResultList = Lists.newArrayList();
        nameResultMap.forEach((uuid, nameResult) -> {
            nameResultList.add(nameResult);
        });
        return nameResultList;
    }

    private void loadNameStorages() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM nameStorage");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;
                String playerName = resultSet.getString("name");
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));

                NameResult nameResult = new NameResult(playerName, uuid);
                this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
                this.nameResultMap.put(uuid, nameResult);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public NameResult getNameResultByName(String playerName) {
        return this.nameResultByNameMap.get(playerName.toLowerCase());
    }

    @Override
    public NameResult getNameResultByUUID(UUID uuid) {
        return this.nameResultMap.get(uuid);
    }

    @Override
    public void updateNameResult(String playerName, UUID uuid, boolean insert) {

        NameResult nameResultByUUID = this.getNameResultByUUID(uuid);
        if (nameResultByUUID == null) {
            NameResult nameResult = new NameResult(playerName, uuid);
            this.nameResultMap.put(uuid, nameResult);
            this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
            return;
        }

        if (nameResultByUUID.getName().equals(playerName)) return;

        this.nameResultByNameMap.remove(getNameResultByUUID(uuid).getName().toLowerCase());
        this.nameResultMap.remove(uuid);

        NameResult nameResult = new NameResult(playerName, uuid);

        this.nameResultMap.put(uuid,nameResult);
        this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
    }
}
