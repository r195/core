package net.ravenix.core.bukkit.command;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class NickAdminCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!player.hasPermission("ravenix.command.nick.admin")) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return true;
        }
        if (args.length != 2) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "Nutze§8: §b/nickadmin <add/remove> <Nickname>");
            return true;
        }
        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        String nickName = args[1];
        if (args[0].equalsIgnoreCase("add")) {
            if (nickProvider.existsNickName(nickName)) {
                player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDieser Nickname existiert bereits.");
                return true;
            }
            nickProvider.addNickName(nickName, true);
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDu hast den Nickname §b" + nickName + " §ahinzugefügt.");
            return true;
        }
        if (args[0].equalsIgnoreCase("remove")) {
            if (!nickProvider.existsNickName(nickName)) {
                player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDieser Nickname existiert nicht.");
                return true;
            }
            nickProvider.removeNickName(nickName, true);
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDu hast den Nickname §b" + nickName + " §aentfernt.");
            return true;
        }
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "Nutze§8: §b/nickadmin <add/remove> <Nickname>");
        return false;
    }
}
