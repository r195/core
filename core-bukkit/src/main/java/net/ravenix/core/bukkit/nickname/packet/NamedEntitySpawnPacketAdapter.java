package net.ravenix.core.bukkit.nickname.packet;

import com.comphenix.packetwrapper.WrapperPlayServerNamedEntitySpawn;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.shared.nickname.NickData;
import org.bukkit.entity.Player;

import java.util.UUID;

public class NamedEntitySpawnPacketAdapter extends PacketAdapter {

    public NamedEntitySpawnPacketAdapter() {
        super(BukkitCore.getInstance(), ListenerPriority.HIGH, PacketType.Play.Server.NAMED_ENTITY_SPAWN);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        final Player player = event.getPlayer();

        final WrapperPlayServerNamedEntitySpawn wrapperPlayServerNamedEntitySpawn = new WrapperPlayServerNamedEntitySpawn(event.getPacket());
        UUID playerUUID = wrapperPlayServerNamedEntitySpawn.getPlayerUUID();
        UUID uuid = player.getUniqueId();

        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();

        if (playerUUID.getLeastSignificantBits() == 0) return;

        if (!player.hasPermission("ravenix.nick.see") && !uuid.equals(playerUUID)) {
            final NickData nickData = nickProvider.getNickData(playerUUID);

            if (nickData != null) {
                wrapperPlayServerNamedEntitySpawn.setPlayerUUID(nickData.getNickedUUID());
            }
        }

        event.setPacket(wrapperPlayServerNamedEntitySpawn.getHandle());
    }
}
