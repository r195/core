package net.ravenix.core.bukkit;

import com.comphenix.protocol.ProtocolLibrary;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.ravenix.core.bukkit.command.*;
import net.ravenix.core.bukkit.listener.*;
import net.ravenix.core.bukkit.listener.report.ReportInventoryClickListener;
import net.ravenix.core.bukkit.mysql.MySQL;
import net.ravenix.core.bukkit.name.NameStorageProvider;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.bukkit.nickname.packet.NamedEntitySpawnPacketAdapter;
import net.ravenix.core.bukkit.nickname.packet.PlayerInfoDataPacketAdapter;
import net.ravenix.core.bukkit.permission.PermissionProvider;
import net.ravenix.core.bukkit.permission.bukkit.PermissionInjector;
import net.ravenix.core.bukkit.player.CorePlayerProvider;
import net.ravenix.core.bukkit.report.ReportProvider;
import net.ravenix.core.bukkit.skin.SkinProvider;
import net.ravenix.core.bukkit.tablist.TabListProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.skin.ISkinProvider;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class BukkitCore
        extends JavaPlugin {

    @Getter
    private static BukkitCore instance;

    @Getter
    private final String prefix = "§8[§b§lR§3§lavenix§8] §7";

    @Getter
    private MySQL mySQL;

    @Getter
    private INameStorageProvider nameStorageProvider;

    @Getter
    private IPermissionProvider permissionProvider;

    @Getter
    private PermissionInjector permissionInjector;

    @Getter
    private TabListProvider tabListProvider;

    @Getter
    private ICorePlayerProvider corePlayerProvider;

    @Getter
    private IReportProvider reportProvider;

    @Getter
    private ISkinProvider skinProvider;

    @Getter
    private NickProvider nickProvider;

    @Override
    public void onEnable() {
        instance = this;

        this.permissionInjector = new PermissionInjector();
        this.tabListProvider = new TabListProvider();
        this.corePlayerProvider = new CorePlayerProvider();
        this.reportProvider = new ReportProvider();

        this.mySQL = new MySQL();
        this.mySQL.connect();

        this.nameStorageProvider = new NameStorageProvider(this.mySQL);
        this.permissionProvider = new PermissionProvider(this.mySQL);
        this.skinProvider = new SkinProvider(this.mySQL);
        this.nickProvider = new NickProvider(this.mySQL);

        loadListenersAndCommands();
        requestData();
    }

    @Override
    public void onDisable() {
        this.mySQL.disconnect();

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.kickPlayer(BukkitCore.getInstance().getPrefix() + "§cDu §cwurdest §cin §ceine §cLobby §cgesendet, §cda §cdieser §cServer §cgestoppt §cwurde.");
        });

    }

    private void loadListenersAndCommands() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new NamedEntitySpawnPacketAdapter());
        ProtocolLibrary.getProtocolManager().addPacketListener(new PlayerInfoDataPacketAdapter());

        getCommand("reports").setExecutor(new ReportsCommand());
        getCommand("nick").setExecutor(new NickCommand());
        getCommand("nickadmin").setExecutor(new NickAdminCommand());
        getCommand("nicklist").setExecutor(new NicklistCommand());
        getCommand("id").setExecutor(new IDCommand());

        PluginManager pluginManager = Bukkit.getPluginManager();
        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());

        pluginManager.registerEvents(new TabListListener(), this);
        pluginManager.registerEvents(new ReportInventoryClickListener(), this);
        pluginManager.registerEvents(new PlayerJoinListener(), this);
        pluginManager.registerEvents(new PermissionInjector(), this);
        pluginManager.registerEvents(new PlayerAsyncChatListener(), this);
    }

    private void requestData() {
        JsonDocument jsonDocument = new JsonDocument();
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("dataRequest", "update", jsonDocument);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("requestNickData", "update", jsonDocument);
    }
}
