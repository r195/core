package net.ravenix.core.bukkit.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.bukkit.permission.bukkit.BukkitPermissible;
import net.ravenix.core.bukkit.permission.bukkit.PermissionInjector;
import net.ravenix.core.bukkit.tablist.TabListProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.permission.ExpirationData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.skin.ISkinProvider;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public final class PubSubListener {

    @EventListener
    public void channelMessage(ChannelMessageReceiveEvent event) {
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        IReportProvider reportProvider = BukkitCore.getInstance().getReportProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        ISkinProvider skinProvider = BukkitCore.getInstance().getSkinProvider();
        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();

        if (event.getChannel().equalsIgnoreCase("requestNickData")) {
            nickProvider.sendNickDatas();
        }

        if (event.getChannel().equalsIgnoreCase("sendNickDatas")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID nickedSkinUUID = UUID.fromString(jsonDocument.getString("nickedSkinUUID"));
            String nickName = jsonDocument.getString("nickName");
            String playerName = jsonDocument.getString("playerName");

            nickProvider.prepareNicKData(uuid, playerName, nickName, nickedSkinUUID, false);
        }

        if (event.getChannel().equalsIgnoreCase("sendNickData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String nickName = jsonDocument.getString("nickName");
            String playerName = jsonDocument.getString("playerName");
            UUID nickedSkinUUID = UUID.fromString(jsonDocument.getString("nickedSkinUUID"));

            Player player = Bukkit.getPlayer(uuid);
            if (player != null)
                player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDein Nickname lautet§8: §7" + nickName);

            nickProvider.prepareNicKData(uuid, playerName, nickName, nickedSkinUUID,false);
        }

        if (event.getChannel().equalsIgnoreCase("removeNickData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            nickProvider.removeNickData(uuid, false);
        }

        if (event.getChannel().equalsIgnoreCase("sendNickname")) {
            JsonDocument jsonDocument = event.getData();
            String nickName = jsonDocument.getString("nickName");
            boolean removed = jsonDocument.getBoolean("removed");
            if (removed) {
                nickProvider.removeNickName(nickName, false);
                return;
            }
            nickProvider.addNickName(nickName, false);
        }

        if (event.getChannel().equalsIgnoreCase("updateSpectator")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));
            UUID spectator = UUID.fromString(jsonDocument.getString("spectator"));

            IReport report = reportProvider.getReport(suspect);
            if (report != null)
                report.setSpectator(spectator);
        }

        if (event.getChannel().equalsIgnoreCase("skinUpdate")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String skinValue = jsonDocument.getString("skinValue");
            String skinSignature = jsonDocument.getString("skinSignature");

            skinProvider.updateSkin(uuid, skinValue, skinSignature, false, false);
        }

        if (event.getChannel().equalsIgnoreCase("unprocessedReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));

            reportProvider.getReport(suspect).setSpectator(null);
        }

        if (event.getChannel().equalsIgnoreCase("denyReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));

            reportProvider.deleteReport(suspect, false);
        }

        if (event.getChannel().equalsIgnoreCase("deleteReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            reportProvider.deleteReport(uuid, false);
        }

        if (event.getChannel().equalsIgnoreCase("reportPlayer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID reporter = UUID.fromString(jsonDocument.getString("reporter"));
            String server = jsonDocument.getString("server");
            String reason = jsonDocument.getString("reason");

            String spectatorString = jsonDocument.getString("spectator");
            UUID spectator;
            if (spectatorString.equalsIgnoreCase("none")) {
                spectator = null;
            } else {
                spectator = UUID.fromString(spectatorString);
            }

            reportProvider.reportPlayer(uuid, reporter, reason, server, spectator,false);
        }

        if (event.getChannel().equalsIgnoreCase("reportData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID reporter = UUID.fromString(jsonDocument.getString("reporter"));
            String spectatorString = jsonDocument.getString("spectator");
            UUID spectator;
            if (spectatorString.equalsIgnoreCase("none")) {
                spectator = null;
            } else {
                spectator = UUID.fromString(spectatorString);
            }
            String server = jsonDocument.getString("server");
            String reason = jsonDocument.getString("reason");

            if (reportProvider.isReported(uuid)) return;

            reportProvider.reportPlayer(uuid, reporter, reason, server, spectator, false);
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerQuit")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(uuid);
            if (corePlayer == null) return;

            corePlayerProvider.corePlayerDisconnect(uuid, false);
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerSwitchServer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String server = jsonDocument.getString("server");

            corePlayerProvider.switchServer(uuid, server, false);
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerJoined")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String server = jsonDocument.getString("server");
            String proxy = jsonDocument.getString("proxy");

            if (corePlayerProvider.getCorePlayer(uuid) != null) return;

            corePlayerProvider.corePlayerConnect(uuid, server, proxy, false);
        }

        if (event.getChannel().equalsIgnoreCase("permissionUpdate")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            boolean add = jsonDocument.getBoolean("add");
            String groupName = jsonDocument.getString("groupName");
            long expiration = jsonDocument.getLong("expiration");

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            IPermissionGroup permissionGroup = permissionProvider.getPermissionGroup(groupName);
            if (add) {
                if (permissionUser.getPermissionGroups().contains(permissionGroup)) return;

                permissionUser.addPermissionGroup(permissionGroup, new ExpirationData(permissionGroup, expiration));

                PermissionInjector permissionInjector = BukkitCore.getInstance().getPermissionInjector();
                BukkitPermissible permissible = permissionInjector.getPermissibleMap().get(uuid);
                permissible.recalculatePermissions(uuid);

                TabListProvider tabListProvider = BukkitCore.getInstance().getTabListProvider();
                Bukkit.getScheduler().runTaskLaterAsynchronously(BukkitCore.getInstance(), tabListProvider::reSetAll, 5);
                return;
            }
            if (!permissionUser.getPermissionGroups().contains(permissionGroup)) return;

            permissionUser.removePermissionGroup(permissionGroup);
            PermissionInjector permissionInjector = BukkitCore.getInstance().getPermissionInjector();
            BukkitPermissible permissible = permissionInjector.getPermissibleMap().get(uuid);
            permissible.recalculatePermissions(uuid);

            TabListProvider tabListProvider = BukkitCore.getInstance().getTabListProvider();

            tabListProvider.reSetAll();
        }
        if (event.getChannel().equalsIgnoreCase("createPermissionUser")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            if (permissionProvider.getPermissionUser(uuid) != null) return;

            permissionProvider.createPermissionUser(uuid, false);
        }
        if (event.getChannel().equalsIgnoreCase("nameStorage")) {
            JsonDocument jsonDocument = event.getData();
            String name = jsonDocument.getString("name");
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            nameStorageProvider.updateNameResult(name, uuid, false);
        }
    }
}
