package net.ravenix.core.bukkit.command;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

public final class NicklistCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!player.hasPermission("ravenix.command.nick.list")) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return true;
        }
        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        Map<UUID, NickData> cachedNickDatas = nickProvider.getCachedNickDatas();
        if (cachedNickDatas.size() == 0) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§cMomentan ist kein Spieler genickt!");
            return true;
        }
        ICorePlayerProvider corePlayerProvider = BukkitCore.getInstance().getCorePlayerProvider();
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
        player.sendMessage("§8");
        cachedNickDatas.forEach((uuid, nickData) -> {
            player.sendMessage("§8- "
                    + permissionProvider.getPermissionUser(uuid).getHighestGroup().getPrefix(PrefixType.DISPLAY)
                    + nameStorageProvider.getNameResultByUUID(uuid).getName() + " §8» §7" + nickData.getNickName() + " §8(§b" + corePlayerProvider.getCorePlayer(uuid).getServer() + "§8)");
        });
        player.sendMessage("§8");
        if (cachedNickDatas.size() == 1) {
            player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Momentan ist §bein §7Spieler genickt.");
            return true;
        }
        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§7Momentan sind §b" + cachedNickDatas.size() + " §7Spieler genickt.");
        return false;
    }
}
