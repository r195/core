package net.ravenix.core.bukkit.report;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.ChatMessageType;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.report.Report;
import net.ravenix.core.shared.report.ReportReason;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class ReportProvider implements IReportProvider {

    private final Map<UUID, IReport> reportMap = Maps.newHashMap();

    public ReportProvider() {
        actionbarThread();
    }

    @Override
    public ReportReason getReportCause(String name) {
        return null;
    }

    @Override
    public IReport getReport(UUID suspect) {
        return this.reportMap.get(suspect);
    }

    @Override
    public boolean isReported(UUID suspect) {
        return this.reportMap.containsKey(suspect);
    }

    @Override
    public void reportPlayer(UUID suspect, UUID reporter, String reason, String server, UUID spectator, boolean send) {
        IReport report = new Report(suspect, reporter, reason, server, spectator);

        this.reportMap.put(suspect, report);
    }

    @Override
    public void deleteReport(UUID suspect, boolean send) {
        this.reportMap.remove(suspect);
    }

    @Override
    public List<IReport> getReports() {
        List<IReport> reports = Lists.newArrayList();
        this.reportMap.forEach((uuid, iReport) -> {
            reports.add(iReport);
        });
        return reports;
    }

    @Override
    public List<IReport> getOpenReports() {
        List<IReport> reports = Lists.newArrayList();
        this.reportMap.forEach((uuid, iReport) -> {
            if (iReport.getSpectator() == null)
                reports.add(iReport);
        });
        return reports;
    }

    @Override
    public void sendOpenReportData() {
        this.reportMap.forEach((uuid, iReport) -> {
            JsonDocument jsonDocument = new JsonDocument();
            jsonDocument.append("uuid", iReport.getSuspect().toString());
            jsonDocument.append("reporter", iReport.getReporter().toString());
            jsonDocument.append("server", iReport.getServer());
            jsonDocument.append("reason", iReport.getReason());
            if (iReport.getSpectator() == null) {
                jsonDocument.append("spectator", "none");
            } else {
                jsonDocument.append("spectator", iReport.getSpectator());
            }
            CloudNetDriver.getInstance().getMessenger().sendChannelMessage("reportData", "update", jsonDocument);
        });
    }

    private void actionbarThread() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitCore.getInstance(), () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                if (getOpenReports().size() > 0) {
                    if (player.hasPermission("ravenix.command.reports")) {
                        if (getOpenReports().size() == 1) {
                            sendActionbar(player, "§7Es ist derzeit §bein §7Report offen!");
                        } else {
                            sendActionbar(player, "§7Es ist derzeit §b" + getOpenReports().size() + " §7Reports offen!");
                        }
                    }
                }
            });
        }, 0L, 20L);
    }
    private void sendActionbar(Player player, String message) {
        PacketPlayOutChat packet = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + message + "\"}"), (byte) 2);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
    }
}

