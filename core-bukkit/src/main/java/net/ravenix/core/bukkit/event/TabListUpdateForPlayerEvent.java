package net.ravenix.core.bukkit.event;

import lombok.Getter;
import net.ravenix.core.bukkit.tablist.TabList;
import net.ravenix.core.shared.permission.PrefixType;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

@Getter
public class TabListUpdateForPlayerEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Map<PrefixType, Map<String, String>> prefixes = new HashMap<>();
    private final Map<PrefixType, Map<String, String>> suffixes = new HashMap<>();

    private final Player player;
    private final Player viewer;
    private final TabList tabList;
    private UUID customUniqueId = null;
    private String customName = null;
    private int position = 0;

    private boolean cancelled = false;

    public TabListUpdateForPlayerEvent(Player player, Player viewer, TabList tabList) {
        this.player = player;
        this.viewer = viewer;
        this.tabList = tabList;
    }

    public void setPrefix(PrefixType prefixType, String key, String prefix) {
        this.prefixes.computeIfAbsent(prefixType, prefixType1 -> new TreeMap<>())
                .put(key, prefix);
    }

    public void removePrefix(PrefixType prefixType, String key) {
        this.prefixes.computeIfAbsent(prefixType, prefixType1 -> new TreeMap<>())
                .remove(key);
    }

    public void setSuffix(PrefixType prefixType, String key, String suffix) {
        this.suffixes.computeIfAbsent(prefixType, prefixType1 -> new TreeMap<>())
                .put(key, suffix);
    }

    public void removeSuffix(PrefixType prefixType, String key) {
        this.suffixes.computeIfAbsent(prefixType, prefixType1 -> new TreeMap<>())
                .remove(key);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void setCustomUniqueId(UUID customUniqueId) {
        this.customUniqueId = customUniqueId;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}