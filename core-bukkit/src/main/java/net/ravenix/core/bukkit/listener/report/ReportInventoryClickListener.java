package net.ravenix.core.bukkit.listener.report;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.inventory.InventoryUtil;
import net.ravenix.core.bukkit.util.ItemBuilder;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.report.Report;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

public final class ReportInventoryClickListener
        implements Listener {

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) return;

        if (event.getCurrentItem().getItemMeta() == null) return;

        if (event.getClickedInventory() == null) return;

        if (event.getClickedInventory().getTitle().equalsIgnoreCase("§bReports")) {
            event.setCancelled(true);
            if (event.getWhoClicked() instanceof Player) {
                Player player = (Player) event.getWhoClicked();
                if (event.isLeftClick()) {
                    if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                        String suspectName = playerName(event.getCurrentItem().getItemMeta().getDisplayName());
                        InventoryUtil.openEditInventory(player, suspectName);
                    }
                }
            }
        }

        if (event.getClickedInventory().getTitle().startsWith("§bReports §8» ")) {
            event.setCancelled(true);
            if (event.getWhoClicked() instanceof Player) {
                Player player = (Player) event.getWhoClicked();
                if (event.isLeftClick()) {
                    String suspectName = playerNameReplaceColor(event.getClickedInventory().getTitle().replace("§bReports §8» ", ""));
                    if (event.getCurrentItem().getType().equals(Material.ENDER_PEARL)) {
                        player.closeInventory();
                        executeCommand(player.getUniqueId(), "goto " + suspectName);
                    }
                    if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
                        player.closeInventory();
                        executeCommand(player.getUniqueId(), "pi " + suspectName);
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cReport ablehnen")) {
                        player.closeInventory();
                        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
                        NameResult nameResultByName = nameStorageProvider.getNameResultByName(suspectName);
                        denyReport(player.getUniqueId(), nameResultByName.getUuid());
                        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDu hast den Report erfolgreich geschlossen.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eUnbearbeitet lassen")) {
                        player.closeInventory();
                        INameStorageProvider nameStorageProvider = BukkitCore.getInstance().getNameStorageProvider();
                        NameResult nameResultByName = nameStorageProvider.getNameResultByName(suspectName);
                        unprocessedReport(player.getUniqueId(), nameResultByName.getUuid());
                        player.sendMessage(BukkitCore.getInstance().getPrefix() + "§aDu hast den Report als §eunbearbeitet §amarkiert.");
                    }
                }
            }
        }
    }

    private void unprocessedReport(UUID uuid, UUID suspect) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("suspect", suspect);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("unprocessedReport", "update", jsonDocument);
    }

    private void denyReport(UUID uuid, UUID suspect) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("suspect", suspect);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("denyReport", "update", jsonDocument);
    }

    private void executeCommand(UUID uuid, String command) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid);
        jsonDocument.append("command", command);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("executeCommand", "update", jsonDocument);
    }

    private String playerNameReplaceColor(String color) {
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        for (IPermissionGroup permissionGroup : permissionProvider.getPermissionGroups()) {
            if (color.contains(permissionGroup.getColor()))
                color = color.replace(permissionGroup.getColor(), "");
        }
        return color;
    }

    private String playerName(String displayName) {
        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();
        for (IPermissionGroup permissionGroup : permissionProvider.getPermissionGroups()) {
            if (displayName.contains(permissionGroup.getPrefix(PrefixType.DISPLAY)))
                displayName = displayName.replace(permissionGroup.getPrefix(PrefixType.DISPLAY), "");
        }
        return displayName;
    }
}
