package net.ravenix.core.bukkit.listener;

import net.ravenix.core.bukkit.BukkitCore;
import net.ravenix.core.bukkit.event.TabListUpdateForPlayerEvent;
import net.ravenix.core.bukkit.nickname.NickProvider;
import net.ravenix.core.shared.nickname.NickData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

public final class TabListListener implements Listener {

    @EventHandler
    public void handleTabList(TabListUpdateForPlayerEvent event) {
        Player player = event.getPlayer();
        if (event.getViewer() == null || player == null || !event.getViewer().isOnline() || !player.isOnline())
            return;

        IPermissionProvider permissionProvider = BukkitCore.getInstance().getPermissionProvider();

        NickProvider nickProvider = BukkitCore.getInstance().getNickProvider();
        final NickData nickData = nickProvider.getNickData(player.getUniqueId());

        final boolean canSee = event.getViewer().hasPermission("ravenix.nick.see");

        final IPermissionUser permissionUser = permissionProvider.getPermissionUser(player.getUniqueId());
        final IPermissionGroup primaryGroup = (nickData != null && !event.getViewer().getUniqueId().equals(event.getPlayer().getUniqueId()) && !canSee)
                ? permissionProvider.getDefaultGroup()
                : permissionUser.getHighestGroup();

        event.setPrefix(PrefixType.NAMETAG, "0000rank",primaryGroup.getPrefix(PrefixType.NAMETAG));
        event.setPrefix(PrefixType.TAB, "0000rank", primaryGroup.getPrefix(PrefixType.TAB));

        event.setPosition(primaryGroup.getPriority());

        if (nickData != null) {
            event.setCustomUniqueId(canSee ? null : nickData.getNickedUUID());
            event.setCustomName(canSee ? null : nickData.getNickName());

            if (canSee) {
                event.setSuffix(PrefixType.TAB, "0001nick", " &8[&5✐&8]");
                event.setSuffix(PrefixType.NAMETAG, "0001nick", " &8[&5✐&8]");
            }
        }

    }
}