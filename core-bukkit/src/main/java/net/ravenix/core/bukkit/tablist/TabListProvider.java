package net.ravenix.core.bukkit.tablist;

import net.ravenix.core.shared.tablist.IInstanceCreator;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TabListProvider {

    private final Map<UUID, TabList> tabListMap = new ConcurrentHashMap<>();

    public IInstanceCreator<UUID, TabList> getCreator() {
        return TabList::new;
    }

    public void createForPlayer(Player player) {
        final TabList tabList = this.getCreator().create(player.getUniqueId());
        this.tabListMap.put(player.getUniqueId(), tabList);
    }

    public void updateAll() {
        this.tabListMap.values().forEach(TabList::update);
    }

    public void reSetAll() {
        this.tabListMap.values().forEach(TabList::reSet);
    }

    public TabList get(UUID uuid) {
        return this.tabListMap.computeIfAbsent(uuid, uuid1 -> this.getCreator().create(uuid1));
    }

    public Collection<TabList> getAll() {
        return this.tabListMap.values();
    }
}
