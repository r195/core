package net.ravenix.core.bungee.chatfilter;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.chatfilter.IChatProvider;
import net.ravenix.core.shared.chatfilter.Word;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.Punishment;
import net.ravenix.core.shared.punishment.PunishmentType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class ChatProvider implements IChatProvider {

    private final Map<String, Word> forbiddenWords = Maps.newHashMap();

    private final MySQL mySQL;

    public ChatProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadForbiddenWords();
    }

    @Override
    public List<Word> getForbiddenWords() {
        List<Word> list = Lists.newArrayList();
        forbiddenWords.forEach((s, word) -> {
            list.add(word);
        });
        return list;
    }

    @Override
    public boolean isForbidden(String message) {
        for (Word forbiddenWord : this.getForbiddenWords()) {
            if (message.toLowerCase().contains(forbiddenWord.getWord().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Word getForbiddenWord(String message) {
        for (Word forbiddenWords : this.getForbiddenWords()) {
            if (message.toLowerCase().contains(forbiddenWords.getWord())) {
                return forbiddenWords;
            }
        }
        return null;
    }

    @Override
    public void addForbiddenWord(String string, boolean autoMute) {
        Word word = new Word(string, autoMute);
        this.forbiddenWords.put(string.toLowerCase(), word);
    }

    @Override
    public void handleVerbose(UUID uuid, String writtenMessage, String server, boolean send) {
        if (send) {
            Word word = getForbiddenWord(writtenMessage);
            if (word == null) return;

            if (word.isAutoMute()) {
                IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
                Punishment punishment = punishmentProvider.getPunishment("chat");
                punishmentProvider.mutePlayer(uuid, null, punishment.getDisplay(), punishment.getPoints(), true);
            }

            sendVerbose(uuid, writtenMessage, server, word);
        }
    }

    private void sendVerbose(UUID uuid, String writtenMessage, String server, Word forbiddenWord) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("writtenMessage", writtenMessage);
        jsonDocument.append("server", server);
        jsonDocument.append("autoMute", forbiddenWord.isAutoMute());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("chatfilter", "update", jsonDocument);
    }

    private void loadForbiddenWords() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM chatfilter");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;

                String wordString = resultSet.getString("word");
                boolean autoMute = resultSet.getBoolean("autoMute");

                Word word = new Word(wordString, autoMute);
                this.forbiddenWords.put(wordString.toLowerCase(), word);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

