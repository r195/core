package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.PunishmentLog;
import net.ravenix.core.shared.punishment.PunishmentType;
import net.ravenix.core.shared.util.TimeFormat;

import java.util.List;

public final class MuteLogCommand extends Command {

    public MuteLogCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if (!player.hasPermission("ravenix.command.punishlog")) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length == 0) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/mutelog <Name> [Seite]");
            return;
        }
        int page = 1;
        if (args.length == 2) {
            try {
                page = Integer.parseInt(args[1]);
            } catch (NumberFormatException ignored) {
                player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cBitte gebe eine gültige Zahl an.");
                return;
            }
        }
        String playerName = args[0];
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);
        if (nameResultByName == null) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        List<PunishmentLog> punishmentLog = punishmentProvider.getPunishmentLog(nameResultByName.getUuid());
        if (punishmentLog == null) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler wurde noch nie gemuted.");
            return;
        }

        List<PunishmentLog> banLogs = Lists.newArrayList();
        punishmentLog.forEach(punishmentLog1 -> {
            if (punishmentLog1.getPunishmentType().equals(PunishmentType.MUTE)) {
                banLogs.add(punishmentLog1);
            }
        });

        int banLogSize = banLogs.size();
        if (banLogSize == 0) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler wurde noch nie gemuted.");
            return;
        }

        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResultByName.getUuid());

        List<PunishmentLog> punishmentLogList;
        if (page > 1 && banLogSize < page * 3) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDiese Seite existiert nicht.");
            return;
        }
        if (page == 1) {
            if (banLogSize < 6) {
                punishmentLogList = banLogs.subList(0, banLogSize);
            } else {
                punishmentLogList = banLogs.subList(0, 6);
            }
        } else {
            int endIndex = 6 * page;
            int startIndex = endIndex - 6;
            if (startIndex > banLogSize) {
                player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDiese Seite existiert nicht.");
                return;
            }
            if (banLogSize < 6 * page) {
                punishmentLogList = banLogs.subList(startIndex, banLogSize);
            } else {
                punishmentLogList = banLogs.subList(startIndex, endIndex);
            }
        }
        player.sendMessage(BungeeCore.getInstance().getPrefix() + permissionUser.getHighestGroup().getColor() + nameResultByName.getName() + " §7hat insgesamt §b" + banLogSize + " Mute§7.");
        punishmentLogList.forEach(punishmentLog1 -> {
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(punishmentLog1.getExecutor());
            IPermissionUser executor = permissionProvider.getPermissionUser(punishmentLog1.getExecutor());
            TextComponent log = new TextComponent();
            log.setText("§8[§b" + TimeFormat.format(punishmentLog1.getTimestamp()) + "§8] §7Grund: §b" + punishmentLog1.getReason());
            if (player.hasPermission("ravenix.command.unban")) {
                log.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§7Dauer§8: §b" + TimeFormat.format(punishmentLog1.getEnd())
                        + "\n§7Gemuted von§8: §b" + executor.getHighestGroup().getColor() + nameResultByUUID.getName() + "\n§cKicke, um diesen Mute zu löschen")));
                log.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/deletemutelog "
                        + nameResultByName.getName()
                        + " " + punishmentLog1.getReason()
                        + " " + nameResultByUUID.getName()
                        + " " + punishmentLog1.getTimestamp()
                        + " " + punishmentLog1.getEnd()));
            } else {
                log.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§7Dauer§8: §b" + TimeFormat.format(punishmentLog1.getEnd())
                        + "\n§7Gemuted von§8: §b" + executor.getHighestGroup().getColor() + nameResultByUUID.getName())));
            }
            player.sendMessage(log);
        });
        if (page == 1) {
            if (banLogSize < 6) {
                return;
            }
        } else {
            if (banLogSize < 6 * page) {
                return;
            }
        }
        TextComponent textComponent = new TextComponent();
        textComponent.setText(BungeeCore.getInstance().getPrefix() + "§7Du bist auf der Seite " + page + " §7klicke §7hier, §7um §7die §7nächste §7Seite §7zu §7öffnen.");
        int nextPage = page + 1;
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mutelog " + nameResultByName.getName() + " " +  nextPage));
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§aKlicke hier, um die nächste Seite zu öffnen.")));
        player.sendMessage(textComponent);
    }
}

