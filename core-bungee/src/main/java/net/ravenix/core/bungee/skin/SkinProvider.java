package net.ravenix.core.bungee.skin;

import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.skin.ISkin;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.core.shared.skin.Skin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

public final class SkinProvider implements ISkinProvider {

    private final MySQL mySQL;

    private final Map<UUID, ISkin> skinData = Maps.newHashMap();

    public SkinProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadSkinData();
    }

    @Override
    public ISkin getSkin(UUID uuid) {
        return this.skinData.get(uuid);
    }

    @Override
    public void updateSkin(UUID uuid, String skinValue, String skinSignature, boolean send, boolean insert) {
        ISkin skin;
        if (this.skinData.get(uuid) == null) {
            skin = new Skin(uuid, skinValue, skinSignature);
            this.skinData.put(uuid, skin);
        } else {
            skin = getSkin(uuid);
            if (!skin.getSkinSignature().equalsIgnoreCase(skinSignature) || !skin.getSkinValue().equalsIgnoreCase(skinValue)) {
                skin.setSkinSignature(skinSignature);
                skin.setSkinValue(skinValue);
            }
        }
    }

    @Override
    public ISkin getRandomSkin() {
        return null;
    }

    private void loadSkinData() {
        ResultSet result = this.mySQL.getResult("SELECT * FROM skinData");
        while (true) {
            try {
                assert result != null;
                if (!result.next()) break;

                UUID uuid = UUID.fromString(result.getString("uuid"));
                String skinValue = result.getString("skinValue");
                String skinSignature = result.getString("skinSignature");
                ISkin skin = new Skin(uuid, skinValue, skinSignature);
                this.skinData.put(uuid, skin);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
