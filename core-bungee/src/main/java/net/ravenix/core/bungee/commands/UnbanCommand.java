package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.punishment.BanData;
import net.ravenix.core.shared.punishment.IPunishmentProvider;

public final class UnbanCommand extends Command {

    public UnbanCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.command.unban")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length != 1) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/unban <Name>");
            return;
        }
        String playerName = args[0];
        NameResult nameResult
                = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(playerName);
        if (nameResult == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        BanData banData = punishmentProvider.getBanData(nameResult.getUuid());
        if (banData == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler ist nicht gebannt.");
            return;
        }
        IPermissionUser permissionUser = BungeeCore.getInstance().getPermissionProvider().getPermissionUser(nameResult.getUuid());
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu hast " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §aentbannt.");
        punishmentProvider.unbanPlayer(nameResult.getUuid(), proxiedPlayer.getUniqueId(), true);
    }
}
