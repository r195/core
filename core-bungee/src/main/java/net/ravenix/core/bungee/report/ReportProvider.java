package net.ravenix.core.bungee.report;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.report.Report;
import net.ravenix.core.shared.report.ReportReason;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class ReportProvider implements IReportProvider {

    private final Map<UUID, IReport> reportMap = Maps.newHashMap();

    private final Map<String, ReportReason> reportReasonMap = Maps.newHashMap();

    private final MySQL mySQL;

    public ReportProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadReasons();
    }

    @Override
    public ReportReason getReportCause(String name) {
        return this.reportReasonMap.get(name.toLowerCase());
    }

    @Override
    public IReport getReport(UUID suspect) {
        return this.reportMap.get(suspect);
    }

    @Override
    public boolean isReported(UUID suspect) {
        if (this.reportMap.get(suspect) == null) return false;
        return this.reportMap.containsKey(suspect);
    }

    @Override
    public void reportPlayer(UUID suspect, UUID reporter, String reason, String server, UUID spectator, boolean send) {
        IReport report = new Report(suspect, reporter, reason, server, spectator);

        this.reportMap.put(suspect, report);

        if (send) {
            updateReport(suspect, reporter, reason, server);
        }
    }

    @Override
    public void deleteReport(UUID suspect, boolean send) {
        this.reportMap.remove(suspect);

        if (send) {
            ICorePlayer corePlayer = BungeeCore.getInstance().getCorePlayerProvider().getCorePlayer(suspect);
            boolean offline = corePlayer == null;
            deleteReportCache(suspect, offline);
        }
    }

    @Override
    public List<IReport> getReports() {
        List<IReport> reports = Lists.newArrayList();
        this.reportMap.forEach((uuid, iReport) -> {
            reports.add(iReport);
        });
        return reports;
    }

    @Override
    public List<IReport> getOpenReports() {
        List<IReport> reports = Lists.newArrayList();
        this.reportMap.forEach((uuid, iReport) -> {
            if (iReport.getSpectator() == null)
                reports.add(iReport);
        });
        return reports;
    }

    @Override
    public void sendOpenReportData() {
        this.reportMap.forEach((uuid, iReport) -> {
            JsonDocument jsonDocument = new JsonDocument();
            jsonDocument.append("uuid", iReport.getSuspect().toString());
            jsonDocument.append("reporter", iReport.getReporter().toString());
            jsonDocument.append("server", iReport.getServer());
            jsonDocument.append("reason", iReport.getReason());
            if (iReport.getSpectator() == null) {
                jsonDocument.append("spectator", "none");
            } else {
                jsonDocument.append("spectator", iReport.getSpectator());
            }
            CloudNetDriver.getInstance().getMessenger().sendChannelMessage("reportData", "update", jsonDocument);
        });
    }

    private void deleteReportCache(UUID suspect, boolean offline) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", suspect.toString());
        jsonDocument.append("offline", offline);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("deleteReport", "update", jsonDocument);
    }

    private void updateReport(UUID suspect, UUID reporter, String reason, String server) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", suspect.toString());
        jsonDocument.append("reporter", reporter.toString());
        jsonDocument.append("server", server);
        jsonDocument.append("reason", reason);
        jsonDocument.append("spectator", "none");
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("reportPlayer", "update", jsonDocument);
    }

    private void loadReasons() {
        ResultSet result = this.mySQL.getResult("SELECT * FROM reportReason");
        while (true) {
            try {
                assert result != null;
                if (!result.next()) break;

                String name = result.getString("name");
                ReportReason reportReason = new ReportReason(name);
                this.reportReasonMap.put(name.toLowerCase(), reportReason);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
