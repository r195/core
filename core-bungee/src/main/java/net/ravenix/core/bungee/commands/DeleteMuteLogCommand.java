package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.PunishmentLog;
import net.ravenix.core.shared.punishment.PunishmentType;

import java.util.List;

public final class DeleteMuteLogCommand extends Command {
    public DeleteMuteLogCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();

        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        if (!player.hasPermission("ravenix.command.unban")) {
            player.sendMessage("Unknown command. Type \"/help\" for help.");
            return;
        }

        if (args.length != 5) {
            player.sendMessage("Unknown command. Type \"/help\" for help.");
            return;
        }
        String playerName = args[0];
        String reason = args[1];
        String bannerName = args[2];
        long when = Long.parseLong(args[3]);
        long end = Long.parseLong(args[4]);

        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);
        NameResult executorStatement = nameStorageProvider.getNameResultByName(bannerName);

        List<PunishmentLog> punishmentLogs = punishmentProvider.getPunishmentLog(nameResultByName.getUuid());
        List<PunishmentLog> thePunishment = Lists.newArrayList();
        punishmentLogs.forEach(punishmentLog -> {
            if (punishmentLog.getReason().equalsIgnoreCase(reason)
                    && punishmentLog.getPunishmentType().equals(PunishmentType.MUTE)
                    && punishmentLog.getExecutor().equals(executorStatement.getUuid())
                    && punishmentLog.getTimestamp() == when
                    && punishmentLog.getEnd() == end) {
                thePunishment.add(punishmentLog);
            }
        });

        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResultByName.getUuid());

        PunishmentLog punishmentLog = thePunishment.get(0);
        if (punishmentLog == null) {
            player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Mute-Log existiert nicht mehr.");
            return;
        }
        punishmentProvider.removePunishmentLog(nameResultByName.getUuid(), punishmentLog, true);

        player.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du hast erfolgreich den §b" + punishmentLog.getReason()
                + "§7-§bMute §7von §b" + permissionUser.getHighestGroup().getColor() + nameResultByName.getName() + " §7gelöscht.");
    }
}
