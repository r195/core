package net.ravenix.core.bungee.commands;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;

public final class BroadcastCommand extends Command {

    public BroadcastCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.command.broadcast")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length == 0) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/bc <Nachricht>");
            return;
        }

        int current = 0;
        int max = args.length - 1;
        StringBuilder stringBuilder = new StringBuilder();
        for (String message : args) {
            if (current == max) {
                stringBuilder.append(message);
            } else {
                stringBuilder.append(message).append(" ");
            }
            current++;
        }
        String message = stringBuilder.toString();

        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("message", message);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("broadcast", "update", jsonDocument);
    }
}
