package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;

public final class ProxyPluginsCommand extends Command {

    public ProxyPluginsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!commandSender.hasPermission("*")) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        ProxyServer.getInstance().getPluginManager().getPlugins().forEach(plugin -> {
            commandSender.sendMessage("§b" + plugin.getDescription().getName() +
                    " §8[§b" + (plugin.getDescription().getAuthor() != null ? plugin.getDescription().getAuthor() : "unknown") + "§8] §7" + plugin.getDescription().getVersion());
        });
    }
}
