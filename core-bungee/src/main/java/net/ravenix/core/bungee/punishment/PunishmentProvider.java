package net.ravenix.core.bungee.punishment;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.punishment.*;
import net.ravenix.core.shared.util.SystemUUID;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class PunishmentProvider implements IPunishmentProvider {

    private final MySQL mySQL;

    private final Map<UUID, BanData> banDataMap = Maps.newHashMap();

    private final Map<UUID, MuteData> muteDataMap = Maps.newHashMap();

    private final Map<String, Punishment> punishmentMap = Maps.newHashMap();

    private final Map<UUID, List<PunishmentLog>> punishmentLogMap = Maps.newHashMap();

    public PunishmentProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        loadPunishmentDatas();
        loadPunishmentLogs();
        loadPunishments();
    }

    @Override
    public PunishmentLog getSpecialPunishmentLog(UUID uuid, UUID executor, long end, long timestamp, String reason) {
        List<PunishmentLog> punishmentLog = getPunishmentLog(uuid);
        List<PunishmentLog> thePunishment = Lists.newArrayList();
        punishmentLog.forEach(punishmentLog1 -> {
            if (punishmentLog1.getUuid().equals(uuid)
                    && punishmentLog1.getEnd() == end
                    && punishmentLog1.getTimestamp() == timestamp
                    && punishmentLog1.getReason().equalsIgnoreCase(reason)
                    && punishmentLog1.getExecutor().equals(executor)) {
                thePunishment.add(punishmentLog1);
            }
        });
        if (thePunishment.size() != 0) {
            return thePunishment.get(0);
        }
        return null;
    }

    @Override
    public List<BanData> getBanDatas() {
        List<BanData> banDataList = Lists.newArrayList();
        this.banDataMap.forEach((uuid, banData) -> {
            banDataList.add(banData);
        });
        return banDataList;
    }

    @Override
    public List<MuteData> getMuteDatas() {
        List<MuteData> muteDataList = Lists.newArrayList();
        this.muteDataMap.forEach((uuid, muteData) -> {
            muteDataList.add(muteData);
        });
        return muteDataList;
    }

    @Override
    public Punishment getPunishment(String name) {
        return this.punishmentMap.get(name.toLowerCase());
    }

    @Override
    public List<Punishment> getPunishments() {
        List<Punishment> allPunishments = Lists.newArrayList();
        this.punishmentMap.forEach((string, punishment) -> {
            allPunishments.add(punishment);
        });
        return allPunishments;
    }

    @Override
    public BanData getBanData(UUID uuid) {
        return this.banDataMap.get(uuid);
    }

    @Override
    public MuteData getMuteData(UUID uuid) {
        return this.muteDataMap.get(uuid);
    }

    @Override
    public void removePunishmentLog(UUID uuid, PunishmentLog punishmentLog, boolean send) {
        List<PunishmentLog> punishmentLogs = getPunishmentLog(uuid);
        if (punishmentLogs != null) {
            punishmentLogs.remove(punishmentLog);
        }
        if (send) {
            this.mySQL.update("DELETE FROM punishmentLog WHERE uuid='" + punishmentLog.getUuid().toString() + "' AND timestamp='" + punishmentLog.getTimestamp() + "' AND reason='" + punishmentLog.getReason() + "' AND executor='" + punishmentLog.getExecutor().toString() + "'");
            removePunishmentLog(punishmentLog);
        }
    }

    private void removePunishmentLog(PunishmentLog punishmentLog) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", punishmentLog.getUuid().toString());
        jsonDocument.append("executor", punishmentLog.getExecutor().toString());
        jsonDocument.append("end", punishmentLog.getEnd());
        jsonDocument.append("timestamp", punishmentLog.getTimestamp());
        jsonDocument.append("reason", punishmentLog.getReason());

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("removedPunishmentLog", "update", jsonDocument);
    }

    @Override
    public void addPunishmentLog(UUID uuid, PunishmentLog punishmentLog) {
        List<PunishmentLog> punishmentLogs = getPunishmentLog(uuid);
        if (punishmentLogs == null) {
            List<PunishmentLog> punishmentLogList = Lists.newArrayList();
            punishmentLogList.add(punishmentLog);
            this.punishmentLogMap.put(uuid, punishmentLogList);
        } else {
            if (!punishmentLogs.contains(punishmentLog))
                punishmentLogs.add(punishmentLog);
        }
    }

    @Override
    public List<PunishmentLog> getPunishmentLog(UUID uuid) {
        return this.punishmentLogMap.get(uuid);
    }

    @Override
    public void banPlayer(UUID uuid, UUID executor, String reason, int points, boolean insert) {
        BanData banData = getBanData(uuid);
        if (banData != null) return;

        List<PunishmentLog> punishmentLogs = getPunishmentLog(uuid);

        int insertPoints = points;

        if (executor == null) {
            executor = SystemUUID.getUUID();
        }

        if (punishmentLogs != null) {
            for (PunishmentLog punishmentLog : punishmentLogs) {
                if (punishmentLog.getPunishmentType().equals(PunishmentType.BAN)) {
                    points = points + punishmentLog.getPoints();
                }
            }
        }

        long duration = this.getDuration(points);

        BanData punishmentData = new BanData(uuid, executor, duration, reason, PunishmentType.BAN);
        this.banDataMap.put(uuid, punishmentData);

        if (insert) {
            this.mySQL.update("INSERT INTO punishmentData (uuid,executor,reason,punishType,duration) " +
                    "VALUES ('" + uuid.toString() + "','" + executor.toString() + "','" + reason + "','"
                    + PunishmentType.BAN.name() + "','" + duration + "')");

            this.mySQL.update("INSERT INTO punishmentLog (uuid,executor,timestamp,reason,punishType,points,end) VALUES ('"
                    + uuid.toString() + "','" + executor.toString() + "','"
                    + System.currentTimeMillis() + "','" + reason + "','"
                    + PunishmentType.BAN.name() + "','" + insertPoints + "','" + duration + "')");

            updatePunishmentData(uuid, executor, PunishmentType.BAN, reason, duration, insertPoints, false);

            PunishmentLog punishmentLog = new PunishmentLog(uuid, executor, System.currentTimeMillis(), reason, PunishmentType.BAN, insertPoints, duration);
            addPunishmentLog(punishmentLog);
        }
    }

    @Override
    public void mutePlayer(UUID uuid, UUID executor, String reason, int points, boolean insert) {
        MuteData muteData = getMuteData(uuid);
        if (muteData != null) return;

        List<PunishmentLog> punishmentLogs = getPunishmentLog(uuid);

        int insertPoints = points;

        if (executor == null) {
            executor = SystemUUID.getUUID();
        }

        if (punishmentLogs != null) {
            for (PunishmentLog punishmentLog : punishmentLogs) {
                if (punishmentLog.getPunishmentType().equals(PunishmentType.MUTE)) {
                    points = points + punishmentLog.getPoints();
                }
            }
        }

        long duration = this.getDuration(points);

        MuteData punishmentData = new MuteData(uuid, executor, duration, reason, PunishmentType.MUTE);
        this.muteDataMap.put(uuid, punishmentData);

        if (insert) {
            this.mySQL.update("INSERT INTO punishmentData (uuid,executor,reason,punishType,duration) " +
                    "VALUES ('" + uuid.toString() + "','" + executor.toString() + "','" + reason + "','"
                    + PunishmentType.MUTE.name() + "','" + duration + "')");

            this.mySQL.update("INSERT INTO punishmentLog (uuid,executor,timestamp,reason,punishType,points,end) VALUES ('"
                    + uuid.toString() + "','" + executor.toString() + "','"
                    + System.currentTimeMillis() + "','" + reason + "','"
                    + PunishmentType.MUTE.name() + "','" + insertPoints + "','" + duration + "')");

            updatePunishmentData(uuid, executor, PunishmentType.MUTE, reason, duration, insertPoints, false);

            PunishmentLog punishmentLog = new PunishmentLog(uuid, executor, System.currentTimeMillis(), reason, PunishmentType.MUTE, insertPoints, duration);
            addPunishmentLog(punishmentLog);
        }
    }

    @Override
    public void kickPlayer(UUID uuid, UUID executor, String reason, boolean send) {

        if (executor == null) {
            executor = SystemUUID.getUUID();
        }

        if (send) {
            updatePunishmentData(uuid, executor, PunishmentType.KICK, reason, -1, 0, false);
        }

    }

    private long getDuration(int points) {
        long duration;
        if (points >= 100) {
            return -1L;
        }
        if (points >= 80) {
            duration = 15552000000L; // 180
        } else if (points >= 60) {
            duration = 7776000000L; // 90
        } else if (points >= 40) {
            duration = 5184000000L; // 60
        } else if (points >= 20) {
            duration = 2592000000L; // 3ß
        } else if (points >= 15) {
            duration = 604800000L; // 7
        } else if (points >= 10) {
            duration = 259200000L; // 3
        } else {
            duration = 86400000L; // 1
        }
        return duration + System.currentTimeMillis();
    }

    @Override
    public void unbanPlayer(UUID uuid, UUID executor, boolean delete) {

        BanData banData = getBanData(uuid);
        if (delete) {
            this.mySQL.update("DELETE FROM punishmentData WHERE uuid='" + uuid.toString()
                    + "' AND punishType='" + banData.getPunishmentType().name() + "'");

            updatePunishmentData(uuid, executor, PunishmentType.BAN, null, 0L, 0, true);
        }

        this.banDataMap.remove(uuid);
    }

    @Override
    public void unmutePlayer(UUID uuid, UUID executor, boolean delete) {
        MuteData muteData = getMuteData(uuid);
        if (delete) {
            this.mySQL.update("DELETE FROM punishmentData WHERE uuid='" + uuid.toString()
                    + "' AND punishType='" + muteData.getPunishmentType().name() + "'");

            updatePunishmentData(uuid, executor, PunishmentType.MUTE, "null", 0L, 0, true);
        }

        this.muteDataMap.remove(uuid);
    }


    @Override
    public boolean checkForBannedAccount(UUID uuid) {
        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();
        String ipAddress = ipProvider.getIPAddress(uuid);

        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        List<UUID> notBannedAccounts = Lists.newArrayList();
        List<UUID> bannedAccounts = Lists.newArrayList();
        ipProvider.getAllIPs().forEach((checkedUUID, checkIPAddress) -> {
            if (ipAddress.equalsIgnoreCase(checkIPAddress)) {
                BanData banData = getBanData(checkedUUID);
                if (banData == null) {
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(checkedUUID);
                    if (!permissionUser.hasPermission("ravenix.ban.bypass")) {
                        notBannedAccounts.add(checkedUUID);
                    }
                } else {
                    if (!banData.getReason().equalsIgnoreCase("Bannumgehung")) {
                        bannedAccounts.add(checkedUUID);
                    }
                }
            }
        });

        if (bannedAccounts.size() > 0 && notBannedAccounts.size() > 0) {
            notBannedAccounts.forEach(checkedUUID -> {
                banPlayer(checkedUUID, SystemUUID.getUUID(), "Bannumgehung", 100, true);
            });
            return true;
        }
        return false;
    }

    @Override
    public boolean checkForMutedAccount(UUID uuid) {
        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();
        String ipAddress = ipProvider.getIPAddress(uuid);

        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        List<UUID> notBannedAccounts = Lists.newArrayList();
        List<UUID> bannedAccounts = Lists.newArrayList();
        ipProvider.getAllIPs().forEach((checkedUUID, checkIPAddress) -> {
            if (ipAddress.equalsIgnoreCase(checkIPAddress)) {
                MuteData muteData = getMuteData(checkedUUID);
                if (muteData == null) {
                    IPermissionUser permissionUser = permissionProvider.getPermissionUser(checkedUUID);
                    if (!permissionUser.hasPermission("ravenix.ban.bypass")) {
                        notBannedAccounts.add(checkedUUID);
                    }
                } else {
                    if (!muteData.getReason().equalsIgnoreCase("Muteumgehung")) {
                        bannedAccounts.add(checkedUUID);
                    }
                }
            }
        });

        if (bannedAccounts.size() > 0 && notBannedAccounts.size() > 0) {
            notBannedAccounts.forEach(checkedUUID -> {
                mutePlayer(checkedUUID, SystemUUID.getUUID(), "Muteumgehung", 100, true);
            });
            return true;
        }
        return false;
    }

    private void addPunishmentLog(PunishmentLog punishmentLog) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", punishmentLog.getUuid().toString());
        jsonDocument.append("executor", punishmentLog.getExecutor().toString());
        jsonDocument.append("punishmentType", punishmentLog.getPunishmentType().name());
        jsonDocument.append("reason", punishmentLog.getReason());
        jsonDocument.append("points", punishmentLog.getPoints());
        jsonDocument.append("timestamp", punishmentLog.getTimestamp());
        jsonDocument.append("end", punishmentLog.getEnd());

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("punishmentLog", "update", jsonDocument);
    }


    private void updatePunishmentData(UUID uuid, UUID executor, PunishmentType punishmentType, String reason, long duration, int points, boolean appeal) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("executor", executor.toString());
        jsonDocument.append("punishmentType", punishmentType.name());
        jsonDocument.append("reason", reason);
        jsonDocument.append("duration", duration);
        jsonDocument.append("points", points);
        jsonDocument.append("appeal", appeal);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("punishPlayer", "update", jsonDocument);
    }

    private void loadPunishmentLogs() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM punishmentLog");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;

                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                UUID executor = UUID.fromString(resultSet.getString("executor"));
                long timestamp = resultSet.getLong("timestamp");
                String reason = resultSet.getString("reason");
                PunishmentType punishmentType = PunishmentType.valueOf(resultSet.getString("punishType"));
                int points = resultSet.getInt("points");
                long end = resultSet.getLong("end");

                PunishmentLog punishmentLog = new PunishmentLog(uuid, executor, timestamp, reason, punishmentType, points, end);
                addPunishmentLog(uuid, punishmentLog);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadPunishments() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM punishments");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;

                String name = resultSet.getString("name");
                String display = resultSet.getString("display");
                PunishmentType punishmentType = PunishmentType.valueOf(resultSet.getString("punishType"));
                int duration = resultSet.getInt("points");
                String permission = resultSet.getString("permission");

                Punishment punishment = new Punishment(name, display, punishmentType, duration, permission);
                this.punishmentMap.put(name.toLowerCase(), punishment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadPunishmentDatas() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM punishmentData");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;

                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                UUID executor = UUID.fromString(resultSet.getString("executor"));
                String reason = resultSet.getString("reason");
                PunishmentType punishmentType = PunishmentType.valueOf(resultSet.getString("punishType"));
                long duration = resultSet.getLong("duration");

                if (punishmentType.equals(PunishmentType.BAN)) {
                    BanData banData = new BanData(uuid, executor, duration, reason, punishmentType);
                    this.banDataMap.put(uuid, banData);
                } else {
                    MuteData muteData = new MuteData(uuid, executor, duration, reason, punishmentType);
                    this.muteDataMap.put(uuid, muteData);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
