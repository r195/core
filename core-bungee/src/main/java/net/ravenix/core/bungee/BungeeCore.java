package net.ravenix.core.bungee;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.ravenix.core.bungee.commands.*;
import net.ravenix.core.bungee.ip.IPProvider;
import net.ravenix.core.bungee.listener.*;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.bungee.name.NameStorageProvider;
import net.ravenix.core.bungee.nickname.NickProvider;
import net.ravenix.core.bungee.permission.PermissionProvider;
import net.ravenix.core.bungee.player.CorePlayerProvider;
import net.ravenix.core.bungee.punishment.PunishmentProvider;
import net.ravenix.core.bungee.report.ReportProvider;
import net.ravenix.core.bungee.chatfilter.ChatProvider;
import net.ravenix.core.bungee.skin.SkinProvider;
import net.ravenix.core.bungee.webapi.WebAPIServer;
import net.ravenix.core.shared.chatfilter.IChatProvider;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.nickname.INickProvider;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.core.shared.util.UUIDFetcher;

import java.util.concurrent.TimeUnit;

public final class BungeeCore
        extends Plugin {

    @Getter
    private static BungeeCore instance;

    @Getter
    private final String prefix = "§8[§b§lR§3§lavenix§8] §7";

    @Getter
    private MySQL mySQL;

    @Getter
    private IPermissionProvider permissionProvider;

    @Getter
    private INameStorageProvider nameStorageProvider;

    @Getter
    private IPunishmentProvider punishmentProvider;

    @Getter
    private IReportProvider reportProvider;

    @Getter
    private ICorePlayerProvider corePlayerProvider;

    @Getter
    private ISkinProvider skinProvider;

    @Getter
    private IChatProvider chatProvider;

    @Getter
    private INickProvider nickProvider;

    @Getter
    private IIPProvider ipProvider;

    @Getter
    private UUIDFetcher uuidFetcher;

    @Override
    public void onEnable() {
        instance = this;

        this.mySQL = new MySQL();
        this.mySQL.connect();

        this.skinProvider = new SkinProvider(this.mySQL);
        this.punishmentProvider = new PunishmentProvider(this.mySQL);
        this.reportProvider = new ReportProvider(this.mySQL);
        this.nameStorageProvider = new NameStorageProvider(this.mySQL);
        this.permissionProvider = new PermissionProvider(this.mySQL);
        this.corePlayerProvider = new CorePlayerProvider();
        this.chatProvider = new ChatProvider(this.mySQL);
        this.nickProvider = new NickProvider(this.mySQL);
        this.ipProvider = new IPProvider(this.mySQL);
        this.uuidFetcher = new UUIDFetcher(10);

        loadCommandsAndListeners();
        requestData();
    }

    @Override
    public void onDisable() {
        this.mySQL.disconnect();
        this.uuidFetcher.shutdown();
    }

    private void loadCommandsAndListeners() {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener(this, new PermissionCheckListener());
        pluginManager.registerListener(this, new PostLoginListener());
        pluginManager.registerListener(this, new PreLoginListener());
        pluginManager.registerListener(this, new PlayerChatListener());
        pluginManager.registerListener(this, new PlayerDisconnectListener());

        CloudNetDriver.getInstance().getEventManager().registerListener(new PubSubListener());

        pluginManager.registerCommand(this, new ForumCommand("forum"));
        pluginManager.registerCommand(this, new DeleteBanLogCommand("deletebanlog"));
        pluginManager.registerCommand(this, new MuteLogCommand("deletemutelog"));
        pluginManager.registerCommand(this, new BanLogCommand("banlog"));
        pluginManager.registerCommand(this, new BanLogCommand("baninfo"));
        pluginManager.registerCommand(this, new MuteLogCommand("mutelog"));
        pluginManager.registerCommand(this, new MuteLogCommand("muteinfo"));
        pluginManager.registerCommand(this, new CoreStatsCommand("corestats"));
        pluginManager.registerCommand(this, new RankCommand("rank"));
        pluginManager.registerCommand(this, new RankCommand("rang"));
        pluginManager.registerCommand(this, new BanCommand("ban"));
        pluginManager.registerCommand(this, new MuteCommand("mute"));
        pluginManager.registerCommand(this, new KickCommand("kick"));
        pluginManager.registerCommand(this, new UnbanCommand("unban"));
        pluginManager.registerCommand(this, new UnmuteCommand("unmute"));
        pluginManager.registerCommand(this, new PlayerInfoCommand("playerinfo"));
        pluginManager.registerCommand(this, new PlayerInfoCommand("pi"));
        pluginManager.registerCommand(this, new PlayerInfoCommand("info"));
        pluginManager.registerCommand(this, new ReportCommand("report"));
        pluginManager.registerCommand(this, new JointoCommand("goto"));
        pluginManager.registerCommand(this, new JointoCommand("jointo"));
        pluginManager.registerCommand(this, new TeamChatCommand("tc"));
        pluginManager.registerCommand(this, new TeamChatCommand("teamchat"));
        pluginManager.registerCommand(this, new TeamCommand("team"));
        pluginManager.registerCommand(this, new BroadcastCommand("bc"));
        pluginManager.registerCommand(this, new BroadcastCommand("broadcast"));
        pluginManager.registerCommand(this, new BroadcastCommand("alert"));
        pluginManager.registerCommand(this, new ProxyPluginsCommand("proxyplugins"));
        pluginManager.registerCommand(this, new ProxyPluginsCommand("pp"));

        if (CloudNetDriver.getInstance().getComponentName().equalsIgnoreCase("Proxy-1")) {
            ProxyServer.getInstance().getScheduler().schedule(this, WebAPIServer::startServer, 2, TimeUnit.SECONDS);
        }
    }

    private void requestData() {
        JsonDocument jsonDocument = new JsonDocument();
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("dataRequest", "update", jsonDocument);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("requestNickData", "update", jsonDocument);
    }
}
