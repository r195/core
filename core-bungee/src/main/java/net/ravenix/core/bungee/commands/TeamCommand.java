package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.util.SystemUUID;

import java.util.Comparator;
import java.util.List;

public final class TeamCommand extends Command {

    public TeamCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.team")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        List<IPermissionUser> soredList = Lists.newArrayList();
        permissionProvider.getPermissionGroups().stream().sorted(Comparator.comparingInt(IPermissionGroup::getPriority)).forEach(permissionGroup -> {
            permissionProvider.getPermissionUsers().forEach(iPermissionUser -> {
                if (iPermissionUser.hasPermission("ravenix.team") && !iPermissionUser.getUUID().equals(SystemUUID.getUUID())) {
                    if (iPermissionUser.getHighestGroup().equals(permissionGroup)) {
                        soredList.add(iPermissionUser);
                    }
                }
            });
        });
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aListe aller Teammitglider:");
        soredList.forEach(permissionUser -> {
            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(permissionUser.getUUID());
            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(permissionUser.getUUID());
            proxiedPlayer.sendMessage((corePlayer == null ? "§8[§c✘§8] " : "§8[§a✔§8] ") + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResultByUUID.getName());
        });
    }
}
