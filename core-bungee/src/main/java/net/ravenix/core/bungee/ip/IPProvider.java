package net.ravenix.core.bungee.ip;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.ip.IIPProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class IPProvider implements IIPProvider {

    private final Map<UUID, String> ipAddressMap = Maps.newHashMap();

    private final MySQL mySQL;

    public IPProvider(MySQL mySQL) {
        this.mySQL = mySQL;

        this.createTables();
        this.loadIPAddress();
    }

    @Override
    public Map<UUID, String> getAllIPs() {
        return this.ipAddressMap;
    }

    @Override
    public String getIPAddress(UUID uuid) {
        return this.ipAddressMap.get(uuid);
    }

    @Override
    public List<UUID> getAllAccountsFromIP(String ipAddress, UUID uuid) {
        List<UUID> uuidList = Lists.newArrayList();

        this.ipAddressMap.forEach((uuids, checkIPAddress) -> {
            if (ipAddress.equalsIgnoreCase(checkIPAddress)) {
                if (!uuid.equals(uuids))
                    uuidList.add(uuids);
            }
        });

        return uuidList;
    }

    private void createTables() {
        this.mySQL.update("CREATE TABLE IF NOT EXISTS ipAddress (uuid varchar(36), ipAddress varchar(100))");
    }

    @Override
    public void createIPAddress(UUID uuid, String ipAddress, boolean send) {
        if (getIPAddress(uuid) != null) return;
        this.ipAddressMap.put(uuid, ipAddress);

        if (send) {
            this.mySQL.update("INSERT INTO ipAddress (uuid,ipAddress) VALUES ('" + uuid.toString() + "','" + ipAddress + "')");
            applyIPAddress(uuid, ipAddress);
        }
    }

    @Override
    public void applyIPAdress(UUID uuid, String ipAddress, boolean send) {
        this.ipAddressMap.remove(uuid);
        this.ipAddressMap.put(uuid, ipAddress);

        if (send) {
            this.mySQL.update("UPDATE ipAddress SET ipAddress='" + ipAddress + "' WHERE uuid='" + uuid.toString() + "'");
            applyIPAddress(uuid, ipAddress);
        }
    }

    private void applyIPAddress(UUID uuid, String ipAddress) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("ipAddress", ipAddress);

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateIP", "update", jsonDocument);
    }

    private void loadIPAddress() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM ipAddress");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;

                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                String ipAddress = resultSet.getString("ipAddress");
                this.ipAddressMap.put(uuid, ipAddress);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
