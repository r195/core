package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;

import java.util.Comparator;
import java.util.List;

public final class RankCommand extends Command {

    public RankCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!commandSender.hasPermission("ravenix.command.rank")) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("groups")) {
                List<IPermissionGroup> group = Lists.newArrayList();
                permissionProvider.getPermissionGroups().stream().sorted(Comparator.comparingInt(IPermissionGroup::getPriority).reversed()).forEach(permissionGroup -> {
                    if (commandSender.hasPermission("ravenix.group." + permissionGroup.getName())) {
                        group.add(permissionGroup);
                    }
                });

                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du kannst Folgenede Ränge vergeben§8: §7" + groups(group));
                return;
            }
        }
        if (args.length != 3) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Nutze§8: §b/rank <Name> <add/remove> <Gruppe>");
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Nutze§8: §b/rank groups");
            return;
        }
        String playerName = args[0];
        NameResult nameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(playerName);
        if (nameResult == null) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
        if (permissionUser == null) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        if (args[1].equalsIgnoreCase("add")) {
            String groupName = args[2];
            IPermissionGroup permissionGroup = permissionProvider.getPermissionGroup(groupName);
            if (permissionGroup == null) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDiese Gruppe existiert nicht.");
                return;
            }
            if (!commandSender.hasPermission("ravenix.group." + permissionGroup.getName())) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu hast keine Berechtigung diese Gruppe zu vergeben oder zu entfernen.");
                return;
            }
            if (permissionUser.getPermissionGroups().contains(permissionGroup)) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler hat diese Gruppe bereits.");
                return;
            }
            if (commandSender instanceof ProxiedPlayer) {
                permissionProvider.addPermissionGroup(permissionUser, ((ProxiedPlayer) commandSender).getUniqueId(), permissionGroup, true);
            } else {
                permissionProvider.addPermissionGroup(permissionUser, null, permissionGroup, true);
            }
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du hast dem Spieler "
                    + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7die Gruppe "
                    + permissionGroup.getColoredName() + " §7zugewiesen.");
            return;
        }
        if (args[1].equalsIgnoreCase("remove")) {
            String groupName = args[2];
            IPermissionGroup permissionGroup = permissionProvider.getPermissionGroup(groupName);
            if (permissionGroup == null) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDiese Gruppe existiert nicht.");
                return;
            }
            if (!commandSender.hasPermission("ravenix.group." + permissionGroup.getName())) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu hast keine Berechtigung diese Gruppe zu vergeben oder zu entfernen.");
                return;
            }
            if (!permissionUser.getPermissionGroups().contains(permissionGroup)) {
                commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler hat diese Gruppe nicht.");
                return;
            }
            if (commandSender instanceof ProxiedPlayer) {
                permissionProvider.removePermissionGroup(permissionUser, ((ProxiedPlayer) commandSender).getUniqueId(), permissionGroup);
            } else {
                permissionProvider.removePermissionGroup(permissionUser, null, permissionGroup);
            }
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du hast dem Spieler "
                    + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7die Gruppe "
                    + permissionGroup.getColoredName() + " §7entfernt.");
            return;
        }
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Nutze§8: §b/rank <Name> <add/remove> <Gruppe>");
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Nutze§8: §b/rank groups");
    }

    private String groups(List<IPermissionGroup> permissionGroupList) {
        int current = 0;
        int max = permissionGroupList.size() - 1;
        StringBuilder stringBuilder = new StringBuilder();
        for (IPermissionGroup permissionGroup : permissionGroupList) {
            if (current == max) {
                stringBuilder.append(permissionGroup.getColoredName());
            } else {
                stringBuilder.append(permissionGroup.getColoredName()).append("§8, ");
            }
            current++;
        }
        return stringBuilder.toString();
    }
}
