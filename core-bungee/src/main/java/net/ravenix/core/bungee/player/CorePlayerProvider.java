package net.ravenix.core.bungee.player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class CorePlayerProvider implements ICorePlayerProvider {

    private final Map<UUID, ICorePlayer> corePlayerMap = Maps.newHashMap();

    @Override
    public ICorePlayer getCorePlayer(UUID uuid) {
        return this.corePlayerMap.get(uuid);
    }

    @Override
    public List<ICorePlayer> getAllCorePlayers() {
        List<ICorePlayer> allCorePlayers = Lists.newArrayList();
        this.corePlayerMap.forEach((uuid, iCorePlayer) -> {
            allCorePlayers.add(iCorePlayer);
        });
        return allCorePlayers;
    }

    @Override
    public void switchServer(UUID uuid, String server, boolean send) {
        ICorePlayer corePlayer = getCorePlayer(uuid);
        if (corePlayer != null)
            corePlayer.setServer(server);

        if (send) {
            corePlayerSwitchServer(uuid, server);
        }
    }

    @Override
    public void corePlayerConnect(UUID uuid, String server, String proxy, boolean send) {
        ICorePlayer corePlayer = new CorePlayer(uuid, true, server, proxy);
        this.corePlayerMap.put(uuid, corePlayer);

        if (send) {
            corePlayerJoin(corePlayer);
        }
    }

    @Override
    public void corePlayerDisconnect(UUID uuid, boolean send) {
        this.corePlayerMap.remove(uuid);

        if (send) {
            corePlayerQuit(uuid);
        }
    }

    @Override
    public void sendOnlinePlayerData() {
        this.corePlayerMap.forEach((uuid, iCorePlayer) -> {
            corePlayerJoin(iCorePlayer);
        });
    }

    private void corePlayerSwitchServer(UUID uuid, String server) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        jsonDocument.append("server", server);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("corePlayerSwitchServer", "update", jsonDocument);
    }

    private void corePlayerQuit(UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", uuid.toString());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("corePlayerQuit", "update", jsonDocument);
    }

    private void corePlayerJoin(ICorePlayer corePlayer) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("uuid", corePlayer.getUUID().toString());
        jsonDocument.append("proxy", corePlayer.getProxy());
        jsonDocument.append("server", corePlayer.getServer());
        jsonDocument.append("online", corePlayer.isOnline());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("corePlayerJoined", "update", jsonDocument);
    }

}
