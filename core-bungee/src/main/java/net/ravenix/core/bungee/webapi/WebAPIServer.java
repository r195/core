package net.ravenix.core.bungee.webapi;

import org.eclipse.jetty.server.Server;

public final class WebAPIServer {

    public static void startServer() {
        Server server = new Server(4252);
        server.setHandler(new WebAPIHandler());
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            server.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
