package net.ravenix.core.bungee.name;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.ravenix.core.bungee.mysql.MySQL;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class NameStorageProvider implements INameStorageProvider {

    private final Map<UUID, NameResult> nameResultMap = Maps.newHashMap();
    private final Map<String, NameResult> nameResultByNameMap = Maps.newHashMap();

    private final MySQL mySQL;

    public NameStorageProvider(MySQL mySQL) {
        this.mySQL = mySQL;
        loadNameStorages();
    }

    private void loadNameStorages() {
        ResultSet resultSet = this.mySQL.getResult("SELECT * FROM nameStorage");
        while (true) {
            try {
                assert resultSet != null;
                if (!resultSet.next()) break;
                String playerName = resultSet.getString("name");
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));

                NameResult nameResult = new NameResult(playerName, uuid);
                this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
                this.nameResultMap.put(uuid, nameResult);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<NameResult> getAllNameStorages() {
        List<NameResult> nameResultList = Lists.newArrayList();
        nameResultMap.forEach((uuid, nameResult) -> {
            nameResultList.add(nameResult);
        });
        return nameResultList;
    }

    @Override
    public NameResult getNameResultByName(String playerName) {
        return this.nameResultByNameMap.get(playerName.toLowerCase());
    }

    @Override
    public NameResult getNameResultByUUID(UUID uuid) {
        return this.nameResultMap.get(uuid);
    }

    @Override
    public void updateNameResult(String playerName, UUID uuid, boolean insert) {
        NameResult nameResultByUUID = this.getNameResultByUUID(uuid);
        if (nameResultByUUID == null) {

            if (insert) {
                this.mySQL.update("INSERT INTO nameStorage (name,uuid) VALUES ('" + playerName + "','" + uuid.toString() + "')");
            }

            NameResult nameResult = new NameResult(playerName, uuid);
            this.nameResultMap.put(uuid,nameResult);
            this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
            updateNameResult(playerName, uuid);
            return;
        }

        if (nameResultByUUID.getName().equals(playerName)) return;

        NameResult nameResult = new NameResult(playerName, uuid);

        this.mySQL.update("UPDATE nameStorage SET name='" + playerName + "' WHERE uuid='" + uuid + "'");

        this.nameResultMap.put(uuid,nameResult);
        this.nameResultByNameMap.put(playerName.toLowerCase(), nameResult);
        updateNameResult(playerName, uuid);
    }

    private void updateNameResult(String name, UUID uuid) {
        JsonDocument jsonDocument = new JsonDocument();
        jsonDocument.append("name", name);
        jsonDocument.append("uuid", uuid.toString());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("nameStorage", "update", jsonDocument);
    }
}
