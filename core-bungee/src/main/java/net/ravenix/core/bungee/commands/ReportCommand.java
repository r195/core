package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.nickname.INickProvider;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.report.ReportReason;

public final class ReportCommand extends Command {

    public ReportCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

        if (args.length != 2) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/report <Name> <Grund>");
            return;
        }
        String playerName = args[0];
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        NameResult nameResult
                = nameStorageProvider.getNameResultByName(playerName);
        String reason = args[1];
        IReportProvider reportProvider = BungeeCore.getInstance().getReportProvider();
        ReportReason reportCause = reportProvider.getReportCause(reason);
        INickProvider nickProvider = BungeeCore.getInstance().getNickProvider();
        if (nameResult == null) {
            if (!nickProvider.getUsedNickNames().contains(playerName)) {
                if (reportCause == null) {
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Grund existiert nicht.");
                    return;
                }
                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu hast diesen Spieler erfolgreich gemeldet.");
                return;
            }
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existert nicht.");
            return;
        }
        if (reportCause == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Grund existiert nicht.");
            return;
        }
        ICorePlayer corePlayer = BungeeCore.getInstance().getCorePlayerProvider().getCorePlayer(nameResult.getUuid());
        if (corePlayer == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler ist nicht online.");
            return;
        }
        if (proxiedPlayer.getUniqueId().equals(nameResult.getUuid())) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu kannst dich nicht selber reporten.");
            return;
        }
        if (reportProvider.isReported(nameResult.getUuid())) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler wurde bereits gemeldet.");
            return;
        }
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu hast diesen Spieler erfolgreich gemeldet.");
        String server = corePlayer.getServer();
        reportProvider.reportPlayer(nameResult.getUuid(), proxiedPlayer.getUniqueId(), reportCause.getName(), server, null, true);
    }
}
