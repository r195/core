package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;

public final class ForumCommand extends Command {
    public ForumCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Link zum registrieren: §3https://ravenix.net/register");
    }
}
