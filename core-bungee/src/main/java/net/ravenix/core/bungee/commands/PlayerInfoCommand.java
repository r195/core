package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.punishment.*;
import net.ravenix.core.shared.util.TimeFormat;
import net.ravenix.playtime.bungee.BungeePlaytime;
import net.ravenix.playtime.shared.playtime.IPlaytimeProvider;
import net.ravenix.playtime.shared.user.IPlaytimeUser;
import net.ravenix.playtime.shared.user.PlaytimeUser;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public final class PlayerInfoCommand extends Command {

    public PlayerInfoCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.command.playerinfo")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length != 1) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/pi <Name>");
            return;
        }
        String playerName = args[0];
        NameResult nameResult
                = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(playerName);
        if (nameResult == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();

        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();

        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());

        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("§7Name§8: " + permissionUser.getHighestGroup().getColor() + nameResult.getName());

        TextComponent uuidMessage = new TextComponent();
        uuidMessage.setText("§7UUID§8: §b" + nameResult.getUuid());
        uuidMessage.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, nameResult.getUuid().toString()));

        proxiedPlayer.sendMessage(uuidMessage);

        String ipAddress = ipProvider.getIPAddress(nameResult.getUuid());

        if (proxiedPlayer.hasPermission("ravenix.see.ipaddress")) {
            TextComponent ipAddressMessage = new TextComponent();
            ipAddressMessage.setText("§7IP-Address§8: §b" + ipAddress);
            ipAddressMessage.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, ipAddress));

            proxiedPlayer.sendMessage(ipAddressMessage);
        }

        ICorePlayer corePlayer = BungeeCore.getInstance().getCorePlayerProvider().getCorePlayer(nameResult.getUuid());
        if (corePlayer == null) {
            proxiedPlayer.sendMessage("§7Status§8: §cOffline");
        } else {
            TextComponent onlineMessage = new TextComponent();
            onlineMessage.setText("§7Status§8: §aOnline");
            onlineMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§7Proxy§8: §b" +
                    corePlayer.getProxy() +
                    "\n§7Server§8: §b" + corePlayer.getServer())));
            onlineMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/goto " + nameResult.getName()));
            proxiedPlayer.sendMessage(onlineMessage);
        }

        proxiedPlayer.sendMessage("§7Hauptgruppe§8: " + permissionUser.getHighestGroup().getColoredName() + " §8(§b" + TimeFormat.format(permissionUser.getExpiration(permissionUser.getHighestGroup()).getExpiration()) + "§8)");

        List<IPermissionGroup> permissionGroups = permissionUser.getPermissionGroups();;

        TextComponent ranksMessage = new TextComponent();
        ranksMessage.setText("§7Gruppen: §b" + permissionUser.getPermissionGroups().size());
        ranksMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new Text(groups(permissionUser, permissionGroups.stream().sorted(Comparator.comparingInt(IPermissionGroup::getPriority)).collect(Collectors.toList())))));

        proxiedPlayer.sendMessage(ranksMessage);

        IPlaytimeProvider playtimeProvider = BungeePlaytime.getInstance().getPlaytimeProvider();
        IPlaytimeUser playtimeUser = playtimeProvider.getPlaytimeUser(nameResult.getUuid());
        proxiedPlayer.sendMessage("§7Spielzeit§8: §b" + playtimeUser.getPlaytimeFormat(corePlayer != null));
        proxiedPlayer.sendMessage("§7Erster Login§8: §b" + TimeFormat.format(playtimeUser.getFirstLogin()));
        proxiedPlayer.sendMessage("§7Letzter Login§8: §b" + TimeFormat.format(playtimeUser.getLastLogin()));

        proxiedPlayer.sendMessage("§7Ban-Punkte§8: §b" + getPoints(PunishmentType.BAN, nameResult.getUuid()));
        BanData banData = punishmentProvider.getBanData(nameResult.getUuid());
        if (banData == null) {
            proxiedPlayer.sendMessage("§7Gebannt§8: §cNein");
        } else {
            TextComponent banMessage = new TextComponent();
            banMessage.setText("§7Gebannt: §aJa");
            if (banData.getDuration() == -1) {
                NameResult executorNameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByUUID(banData.getExecutor());
                IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(banData.getExecutor());
                banMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                        "§7Grund§8: §b" + banData.getReason() +
                                "\n§7Gebannt von§8: §b" + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() +
                                "\n§7Dauer§8: §4PERMANENT")));
            } else {
                NameResult executorNameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByUUID(banData.getExecutor());
                IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(banData.getExecutor());
                banMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                        "§7Grund§8: §b" + banData.getReason() +
                                "\n§7Gebannt von§8: §b" + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() +
                                "\n§7Dauer§8: §b" + TimeFormat.format(banData.getDuration()))));
            }
            proxiedPlayer.sendMessage(banMessage);
        }

        proxiedPlayer.sendMessage("§7Mute-Punkte§8: §b" + getPoints(PunishmentType.MUTE, nameResult.getUuid()));
        MuteData muteData = punishmentProvider.getMuteData(nameResult.getUuid());
        if (muteData == null) {
            proxiedPlayer.sendMessage("§7Gemuted§8: §cNein");
        } else {
            TextComponent muteMessage = new TextComponent();
            muteMessage.setText("§7Gemuted: §aJa");
            if (muteData.getDuration() == -1) {
                NameResult executorNameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByUUID(muteData.getExecutor());
                IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(muteData.getExecutor());
                muteMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                        "§7Grund§8: §b" + muteData.getReason() +
                                "\n§7Gemuted von§8: §b" + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() +
                                "\n§7Dauer§8: §4PERMANENT")));
            } else {
                NameResult executorNameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByUUID(muteData.getExecutor());
                IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(muteData.getExecutor());
                muteMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                        "§7Grund§8: §b" + muteData.getReason() +
                                "\n§7Gemuted von§8: §b" + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() +
                                "\n§7Dauer§8: §b" + TimeFormat.format(muteData.getDuration()))));
            }
            proxiedPlayer.sendMessage(muteMessage);
        }
        TextComponent altAccounts = new TextComponent();
        List<UUID> allAccountsFromIP = ipProvider.getAllAccountsFromIP(ipAddress, nameResult.getUuid());
        altAccounts.setText("§7Alts: §b" + allAccountsFromIP.size());
        if (allAccountsFromIP.size() == 0) {
            altAccounts.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new Text("§cKeine Alt-Accounts gefunden")));
        } else {
            altAccounts.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new Text(accounts(allAccountsFromIP))));
        }
        proxiedPlayer.sendMessage(altAccounts);
        proxiedPlayer.sendMessage("");
    }

    private int getPoints(PunishmentType punishmentType, UUID uuid) {
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        int points = 0;

        List<PunishmentLog> punishmentLogs = punishmentProvider.getPunishmentLog(uuid);
        if (punishmentLogs == null) {
            return 0;
        }

        for (PunishmentLog punishmentLog : punishmentLogs) {
            if (punishmentLog.getPunishmentType().equals(punishmentType)) {
                points = points + punishmentLog.getPoints();
            }
        }

        return points;
    }

    private String accounts(List<UUID> uuidList) {
        int current = 0;
        int max = uuidList.size() - 1;
        StringBuilder stringBuilder = new StringBuilder();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();

        for (UUID uuid : uuidList) {
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            if (current == max) {
                MuteData muteData = punishmentProvider.getMuteData(uuid);
                BanData banData = punishmentProvider.getBanData(uuid);
                if (banData != null) {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName()).append(" §8[§cGebannt §8- §b").append(banData.getReason()).append("§8]");
                } else if (muteData != null) {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName()).append(" §8[§cGemuted §8- §b").append(muteData.getReason()).append("§8]");
                } else {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName());
                }
            } else {
                BanData banData = punishmentProvider.getBanData(uuid);
                MuteData muteData = punishmentProvider.getMuteData(uuid);
                if (banData != null) {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName()).append(" §8[§cGebannt §8- §b").append(banData.getReason()).append("§8]").append("\n");
                } else if (muteData != null) {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName()).append(" §8[§cGemuted §8- §b").append(muteData.getReason()).append("§8]").append("\n");
                } else {
                    stringBuilder.append(permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)).append(nameResult.getName()).append("\n");
                }
            }
            current++;
        }
        return stringBuilder.toString();
    }

    private String groups(IPermissionUser permissionUser, List<IPermissionGroup> permissionGroupList) {
        int current = 0;
        int max = permissionGroupList.size() - 1;
        StringBuilder stringBuilder = new StringBuilder();
        for (IPermissionGroup permissionGroup : permissionGroupList) {
            if (current == max) {
                stringBuilder.append(permissionGroup.getColoredName()).append(" §8(§b").append(TimeFormat.format(permissionUser.getExpiration(permissionGroup).getExpiration())).append("§8)");
            } else {
                stringBuilder.append(permissionGroup.getColoredName()).append(" §8(§b").append(TimeFormat.format(permissionUser.getExpiration(permissionGroup).getExpiration())).append("§8)").append("§8, ");
            }
            current++;
        }
        return stringBuilder.toString();
    }

}
