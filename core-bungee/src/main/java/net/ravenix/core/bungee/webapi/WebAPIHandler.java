package net.ravenix.core.bungee.webapi;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.ProxyServer;
import net.ravenix.coinsystem.bungee.BungeeCoins;
import net.ravenix.coinsystem.bungee.coins.CoinProvider;
import net.ravenix.coinsystem.bungee.commands.CoinCommand;
import net.ravenix.coinsystem.shared.coins.ICoinProvider;
import net.ravenix.coinsystem.shared.user.ICoinUser;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.util.DownloadHelper;
import net.ravenix.core.shared.util.SystemUUID;
import net.ravenix.stats.shared.stats.IStatsProvider;
import net.ravenix.statssystem.bungee.BungeeStats;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class WebAPIHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        if (request.getParameter("getStats") != null) {
            String playerName = request.getParameter("playerName");

            INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
            NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);

            if (nameResultByName == null) return;

            String statKey = request.getParameter("statKey");

            IStatsProvider statsProvider = BungeeStats.getInstance().getStatsProvider();
            Integer statValue = statsProvider.getStatValue(nameResultByName.getUuid(), statKey);
            httpServletResponse.getWriter().println(statValue);
        }
        if (request.getParameter("runCommand") != null) {
            String playerName = request.getParameter("forPlayer");
            String[] args = request.getParameter("arguments").split("~");

            INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
            NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);

            IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

            if (args[0].equalsIgnoreCase("rang")) {
                NameResult buyed_something = nameStorageProvider.getNameResultByName(args[1]);
                IPermissionUser permissionUser = permissionProvider.getPermissionUser(buyed_something.getUuid());
                String groupName = args[2];
                long duration = Long.parseLong(args[3]);

                if (duration != -1L) {
                    duration = TimeUnit.DAYS.toMillis(duration) + System.currentTimeMillis();
                }

                IPermissionGroup permissionGroup = permissionProvider.getPermissionGroup(groupName);
                if (permissionGroup == null) return;
                if (duration == -1L) {
                    permissionProvider.addPermissionGroup(permissionUser, nameResultByName.getUuid(), permissionGroup, true);
                } else {
                    permissionProvider.addPermissionGroup(permissionUser, permissionGroup, nameResultByName.getUuid(), duration, true);
                }
            }
            if (args[0].equalsIgnoreCase("coins")) {
                NameResult buyed_something = nameStorageProvider.getNameResultByName(args[2]);
                ICoinProvider coinProvider = BungeeCoins.getInstance().getCoinProvider();

                int coins = Integer.parseInt(args[3]);

                coinProvider.addCoins(buyed_something.getUuid(), coins, CloudNetDriver.getInstance().getComponentName(), true);
            }
        }
        if (request.getParameter("getRankID") != null) {
            String playerName = request.getParameter("playerName");
            INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
            NameResult nameResultByName = nameStorageProvider.getNameResultByName(playerName);
            if (nameResultByName == null) return;
            IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResultByName.getUuid());
            int forumGroupID = getForumGroupID(permissionUser.getHighestGroup());
            updateForumRank(nameResultByName.getUuid(), permissionUser.getHighestGroup());
            httpServletResponse.getWriter().println(forumGroupID);
        }

        httpServletResponse.setContentType("text/html;charset=utf-8");
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);

    }
    private void updateForumRank(UUID uuid, IPermissionGroup permissionGroup) {
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
        String updateRangURLString = "https://ravenix.net/update-rank.php?username=" + nameResultByUUID.getName() + "&rangid=" + getForumGroupID(permissionGroup) + "&pw=cB8trH0mHIav";
        DownloadHelper.openURLConnection(updateRangURLString, new HashMap<>(), inputStream -> {
            try {
                inputStream.getResponseMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
            inputStream.disconnect();
        }, Throwable::printStackTrace);
    }

    private int getForumGroupID(IPermissionGroup permissionGroup) {
        if (permissionGroup.getName().equalsIgnoreCase("Administrator")) {
            return 5;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator+")) {
            return 17;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Developer")) {
            return 6;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Moderator")) {
            return 8;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Content")) {
            return 16;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Developer")) {
            return 7;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Moderator")) {
            return 10;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Content")) {
            return 9;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Moderator")) {
            return 15;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Builder")) {
            return 11;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Designer")) {
            return 11;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Developer")) {
            return 19;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Content")) {
            return 18;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator")) {
            return 12;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Premium")) {
            return 14;
        }
        return 3;
    }
}
