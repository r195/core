package net.ravenix.core.bungee.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.punishment.BanData;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.util.TimeFormat;
import net.ravenix.core.shared.util.UUIDFetcher;

import java.util.UUID;

public final class PreLoginListener
        implements Listener {

    @EventHandler
    public void preLogin(PreLoginEvent event) {
        String name = event.getConnection().getName();

        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();

        NameResult nameResult
                = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(name);

        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();

        UUIDFetcher uuidFetcher = BungeeCore.getInstance().getUuidFetcher();

        if (nameResult == null) {
            UUID uuid = uuidFetcher.fetchUUID(name);
            ipProvider.createIPAddress(uuid, event.getConnection().getAddress().getHostString(), true);
            ipProvider.applyIPAdress(uuid, event.getConnection().getAddress().getHostString(), true);
            punishmentProvider.checkForBannedAccount(uuid);
            return;
        }

        ipProvider.createIPAddress(nameResult.getUuid(), event.getConnection().getAddress().getHostString(), true);
        ipProvider.applyIPAdress(nameResult.getUuid(), event.getConnection().getAddress().getHostString(), true);

        UUID uuid = nameResult.getUuid();

        punishmentProvider.checkForBannedAccount(uuid);

        BanData banData = punishmentProvider.getBanData(uuid);

        if (banData == null) {
            return;
        }

        if (banData.getDuration() != -1) {
            if (banData.getDuration() < System.currentTimeMillis()) {
                punishmentProvider.unbanPlayer(uuid, null, true);
                return;
            }
        }

        event.setCancelled(true);
        if (banData.getDuration() == -1) {
            event.setCancelReason("§8[§3§lR§bavenix§8]" +
                    "\n§cDu wurdest gebannt!" +
                    "\n§c" +
                    "\n§7Grund§8: §b" + banData.getReason() +
                    "\n§7Dauer§8: §4PERMANENT" +
                    "\n" +
                    "\n" +
                    "\n§aDu kannst einen Entbannungsantrag im Forum stellen." +
                    "\n§bravenix.net");
            return;
        }
        event.setCancelReason("§8[§3§lR§bavenix§8]" +
                "\n§cDu wurdest gebannt!" +
                "\n§c" +
                "\n§7Grund§8: §b" + banData.getReason() +
                "\n§7Dauer§8: §b" + TimeFormat.format(banData.getDuration()) +
                "\n" +
                "\n" +
                "\n§aDu kannst einen Entbannungsantrag im Forum stellen." +
                "\n§bravenix.net");
    }

}
