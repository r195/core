package net.ravenix.core.bungee.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.chatfilter.IChatProvider;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.MuteData;
import net.ravenix.core.shared.util.SystemUUID;
import net.ravenix.core.shared.util.TimeFormat;

public final class PlayerChatListener
        implements Listener {

    @EventHandler
    public void playerChat(ChatEvent event) {
        if (event.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) event.getSender();
            IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
            MuteData muteData = punishmentProvider.getMuteData(player.getUniqueId());

            if (event.getMessage().startsWith("/cloud") && !player.hasPermission("*")) {
                player.sendMessage("Unknown command. Type \"/help\" for help.");
                event.setCancelled(true);
                return;
            }

            if (event.getMessage().startsWith("/ghalutghaiuthajghaiuthka ")) {
                String[] args = event.getMessage().split(" ");
                String serverName = args[1];
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(serverName);
                if (serverInfo == null) {
                    player.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Server existiert nicht.");
                    return;
                }
                player.connect(serverInfo);
                event.setCancelled(true);
                return;
            }
            if (muteData == null) {
                punishmentProvider.checkForMutedAccount(player.getUniqueId());
            }
            if (muteData != null) {
                if (muteData.getDuration() != -1) {
                    if (muteData.getDuration() < System.currentTimeMillis()) {
                        punishmentProvider.unmutePlayer(player.getUniqueId(), SystemUUID.getUUID(), true);
                        return;
                    }
                }

                if (!event.getMessage().startsWith("/")
                        || event.getMessage().startsWith("/msg ")
                        || event.getMessage().startsWith("/r ")
                        || event.getMessage().startsWith("/pc ") || event.getMessage().startsWith("/cc ")) {
                    event.setCancelled(true);
                    player.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du wurdest gemuted.");
                    player.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + muteData.getReason());
                    if (muteData.getDuration() == -1) {
                        player.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §4PERMANENT");
                    } else {
                        player.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §b" + TimeFormat.format(muteData.getDuration()));
                    }
                }
                return;
            }
            IChatProvider chatProvider = BungeeCore.getInstance().getChatProvider();
            String message = event.getMessage();
            if (player.hasPermission("ravenix.chat.bypass")) return;

            if (!event.getMessage().startsWith("/")) {
                boolean forbidden = chatProvider.isForbidden(message);
                if (!forbidden) return;
                chatProvider.handleVerbose(player.getUniqueId(), message, player.getServer().getInfo().getName(), true);
            }
        }
    }
}
