package net.ravenix.core.bungee.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.EventPriority;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.nickname.INickProvider;
import net.ravenix.core.shared.permission.ExpirationData;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.PunishmentLog;
import net.ravenix.core.shared.punishment.PunishmentType;
import net.ravenix.core.shared.report.IReport;
import net.ravenix.core.shared.report.IReportProvider;
import net.ravenix.core.shared.skin.ISkinProvider;
import net.ravenix.core.shared.util.DownloadHelper;
import net.ravenix.core.shared.util.TimeFormat;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

public final class PubSubListener {

    @EventListener(priority = EventPriority.HIGHEST)
    public void channelMessage(ChannelMessageReceiveEvent event) {
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        IReportProvider reportProvider = BungeeCore.getInstance().getReportProvider();
        ISkinProvider skinProvider = BungeeCore.getInstance().getSkinProvider();
        INickProvider nickProvider = BungeeCore.getInstance().getNickProvider();
        IIPProvider ipProvider = BungeeCore.getInstance().getIpProvider();

        if (event.getChannel().equalsIgnoreCase("updateIP")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String ipAddress = jsonDocument.getString("ipAddress");

            ipProvider.applyIPAdress(uuid, ipAddress, false);
        }

        if (event.getChannel().equalsIgnoreCase("dataRequest")) {
            corePlayerProvider.sendOnlinePlayerData();
            reportProvider.sendOpenReportData();
        }

        if (event.getChannel().equalsIgnoreCase("chatfilter")) {
            JsonDocument jsonDocument = event.getData();

            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String writtenMessage = jsonDocument.getString("writtenMessage");
            String server = jsonDocument.getString("server");
            boolean autoMute = jsonDocument.getBoolean("autoMute");

            NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            if (!autoMute) {
                TextComponent textComponent = new TextComponent();
                textComponent.setText(BungeeCore.getInstance().getPrefix() + "§a§l[BESTRAFEN]");
                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mute " + nameResultByUUID.getName() + " chat"));

                ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                    if (proxiedPlayer.hasPermission("ravenix.team")) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY)
                                + nameResultByUUID.getName() + " §7wurde vom §4System §7gemeldet.");
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Er schrieb§8: §b" + writtenMessage);
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Auf§8: §b" + server);
                        proxiedPlayer.sendMessage(textComponent);
                    }
                });
                return;
            }
        }

        if (event.getChannel().equalsIgnoreCase("skinUpdate")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String skinValue = jsonDocument.getString("skinValue");
            String skinSignature = jsonDocument.getString("skinSignature");

            skinProvider.updateSkin(uuid, skinValue, skinSignature, false, false);
        }

        if (event.getChannel().equalsIgnoreCase("broadcast")) {
            JsonDocument jsonDocument = event.getData();
            String message = jsonDocument.getString("message");

            ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + ChatColor.translateAlternateColorCodes('&', message));
            });
        }

        if (event.getChannel().equalsIgnoreCase("teamChat")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String message = jsonDocument.getString("message");
            String server = jsonDocument.getString("server");

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);

            ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                if (proxiedPlayer.hasPermission("ravenix.team")) {
                    proxiedPlayer.sendMessage("§8[§e§lT§6§leam§e§lC§6§lhat§8] " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §8(§e" + server + "§8)§8: §7" + message);
                }
            });
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerQuit")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            ICorePlayer corePlayer = corePlayerProvider.getCorePlayer(uuid);
            if (corePlayer == null) return;

            corePlayerProvider.corePlayerDisconnect(uuid, false);
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerSwitchServer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String server = jsonDocument.getString("server");

            corePlayerProvider.switchServer(uuid, server, false);
        }

        if (event.getChannel().equalsIgnoreCase("corePlayerJoined")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String server = jsonDocument.getString("server");
            String proxy = jsonDocument.getString("proxy");

            if (corePlayerProvider.getCorePlayer(uuid) != null) return;

            corePlayerProvider.corePlayerConnect(uuid, server, proxy, false);
        }

        if (event.getChannel().equalsIgnoreCase("deleteReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            boolean offline = jsonDocument.getBoolean("offline");

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);

            reportProvider.deleteReport(uuid, false);

            if (offline) {
                ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                    if (proxiedPlayer.hasPermission("ravenix.team")) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                + "§7Der Report über " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde gelöscht," +
                                " da dieser Spieler nun offline ist.");
                    }
                });
            }
        }

        if (event.getChannel().equalsIgnoreCase("executeCommand")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String command = jsonDocument.getString("command");

            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
            if (proxiedPlayer != null) {
                ProxyServer.getInstance().getPluginManager().dispatchCommand(proxiedPlayer, command);
            }
        }


        if (event.getChannel().equalsIgnoreCase("updateSpectator")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));
            UUID spectator = UUID.fromString(jsonDocument.getString("spectator"));

            IReport report = reportProvider.getReport(suspect);
            if (report != null)
                report.setSpectator(spectator);
        }

        if (event.getChannel().equalsIgnoreCase("reportData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID reporter = UUID.fromString(jsonDocument.getString("reporter"));
            String spectatorString = jsonDocument.getString("spectator");
            UUID spectator;
            if (spectatorString.equalsIgnoreCase("none")) {
                spectator = null;
            } else {
                spectator = UUID.fromString(spectatorString);
            }
            String server = jsonDocument.getString("server");
            String reason = jsonDocument.getString("reason");

            if (reportProvider.isReported(uuid)) return;

            reportProvider.reportPlayer(uuid, reporter, reason, server, spectator, false);
        }

        if (event.getChannel().equalsIgnoreCase("reportPlayer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID reporter = UUID.fromString(jsonDocument.getString("reporter"));
            String spectatorString = jsonDocument.getString("spectator");
            UUID spectator;
            if (spectatorString.equalsIgnoreCase("none")) {
                spectator = null;
            } else {
                spectator = UUID.fromString(spectatorString);
            }
            String server = jsonDocument.getString("server");
            String reason = jsonDocument.getString("reason");

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            NameResult executorNameResult = nameStorageProvider.getNameResultByUUID(reporter);

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(reporter);

            reportProvider.reportPlayer(uuid, reporter, reason, server, spectator, false);

            ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                if (proxiedPlayer.hasPermission("ravenix.command.reports")) {
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                            + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                            + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7gemeldet.");
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + reason);
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Server§8: §b" + server);
                }
            });
        }

        if (event.getChannel().equalsIgnoreCase("unprocessedReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));

            reportProvider.getReport(suspect).setSpectator(null);
        }

        if (event.getChannel().equalsIgnoreCase("denyReport")) {
            JsonDocument jsonDocument = event.getData();
            UUID suspect = UUID.fromString(jsonDocument.getString("suspect"));

            reportProvider.deleteReport(suspect, false);
        }

        if (event.getChannel().equalsIgnoreCase("punishmentLog")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID executor = UUID.fromString(jsonDocument.getString("executor"));
            PunishmentType punishmentType = PunishmentType.valueOf(jsonDocument.getString("punishmentType"));
            String reason = jsonDocument.getString("reason");
            int points = jsonDocument.getInt("points");
            long timestamp = jsonDocument.getLong("timestamp");
            long end = jsonDocument.getLong("end");

            PunishmentLog punishmentLog = new PunishmentLog(uuid, executor, timestamp, reason, punishmentType, points, end);
            punishmentProvider.addPunishmentLog(uuid, punishmentLog);
        }

        if (event.getChannel().equalsIgnoreCase("removedPunishmentLog")) {
            JsonDocument jsonDocument = event.getData();

            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID executor = UUID.fromString(jsonDocument.getString("executor"));
            long end = jsonDocument.getLong("end");
            long timestamp = jsonDocument.getLong("timestamp");
            String reason = jsonDocument.getString("reason");

            PunishmentLog specialPunishmentLog = punishmentProvider.getSpecialPunishmentLog(uuid, executor, end, timestamp, reason);

            punishmentProvider.removePunishmentLog(uuid, specialPunishmentLog, false);
        }

        if (event.getChannel().equalsIgnoreCase("punishPlayer")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID executor = UUID.fromString(jsonDocument.getString("executor"));
            PunishmentType punishmentType = PunishmentType.valueOf(jsonDocument.getString("punishmentType"));
            String reason = jsonDocument.getString("reason");
            long duration = jsonDocument.getLong("duration");
            int points = jsonDocument.getInt("points");
            boolean appeal = jsonDocument.getBoolean("appeal");

            NameResult nameResult = nameStorageProvider.getNameResultByUUID(uuid);
            NameResult executorNameResult = nameStorageProvider.getNameResultByUUID(executor);

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            IPermissionUser executorPermissionUser = permissionProvider.getPermissionUser(executor);

            if (!appeal) {
                if (punishmentType.equals(PunishmentType.MUTE)) {
                    ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                        if (proxiedPlayer.hasPermission("ravenix.team")) {

                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                    + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                                    + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7gemuted.");
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + reason);
                            if (duration == -1) {
                                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §4PERMANENT");
                            } else {
                                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §b" + TimeFormat.format(duration));
                            }
                        }
                    });
                    punishmentProvider.mutePlayer(uuid, executor, reason, points, false);
                    ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
                    if (proxiedPlayer != null) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Du wurdest gemuted.");
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + reason);
                        if (duration == -1) {
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §4PERMANENT");
                        } else {
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §b" + TimeFormat.format(duration));
                        }
                    }
                }

                if (punishmentType.equals(PunishmentType.KICK)) {
                    ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                        if (proxiedPlayer.hasPermission("ravenix.team")) {
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                    + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                                    + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7gekickt.");
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + reason);
                        }
                    });
                    if (ProxyServer.getInstance().getPlayer(uuid) != null) {
                        ProxyServer.getInstance().getPlayer(uuid).disconnect(
                                "§8[§3§lR§bavenix§8]" +
                                        "\n§cDu wurdest gekickt!" +
                                        "\n§c" +
                                        "\n§7Grund§8: §b" + reason +
                                        "\n");
                    }
                }

                if (punishmentType.equals(PunishmentType.BAN)) {
                    ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                        if (proxiedPlayer.hasPermission("ravenix.team")) {
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                    + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                                    + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7gebannt.");
                            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Grund§8: §b" + reason);
                            if (duration == -1) {
                                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §4PERMANENT");
                            } else {
                                proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Dauer§8: §b" + TimeFormat.format(duration));
                            }
                        }
                    });
                    punishmentProvider.banPlayer(uuid, executor, reason, points, false);
                    if (ProxyServer.getInstance().getPlayer(uuid) != null) {
                        if (duration == -1) {
                            ProxyServer.getInstance().getPlayer(uuid).disconnect(
                                    "§8[§3§lR§bavenix§8]" +
                                            "\n§cDu wurdest gebannt!" +
                                            "\n§c" +
                                            "\n§7Grund§8: §b" + reason +
                                            "\n§7Dauer§8: §4PERMANENT" +
                                            "\n" +
                                            "\n" +
                                            "\n§aDu kannst einen Entbannungsantrag im Forum stellen." +
                                            "\n§bravenix.net");
                            return;
                        }
                        ProxyServer.getInstance().getPlayer(uuid).disconnect(
                                "§8[§3§lR§bavenix§8]" +
                                        "\n§cDu wurdest gebannt!" +
                                        "\n§c" +
                                        "\n§7Grund§8: §b" + reason +
                                        "\n§7Dauer§8: §b" + TimeFormat.format(duration) +
                                        "\n" +
                                        "\n" +
                                        "\n§aDu kannst einen Entbannungsantrag im Forum stellen." +
                                        "\n§bravenix.net");
                    }
                }
                return;
            }
            if (punishmentType.equals(PunishmentType.BAN)) {
                punishmentProvider.unbanPlayer(uuid, executor, false);
                ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                    if (proxiedPlayer.hasPermission("ravenix.team")) {
                        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                                + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                                + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7entbannt.");
                    }
                });
                return;
            }
            punishmentProvider.unmutePlayer(uuid, executor, false);
            ProxyServer.getInstance().getPlayers().forEach(proxiedPlayer -> {
                if (proxiedPlayer.hasPermission("ravenix.team")) {
                    proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                            + "§7Der Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §7wurde von "
                            + executorPermissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + executorNameResult.getName() + " §7entmuted.");
                }
            });
            return;
        }
        if (event.getChannel().equalsIgnoreCase("permissionUpdate")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            boolean add = jsonDocument.getBoolean("add");
            String groupName = jsonDocument.getString("groupName");
            long expiration = jsonDocument.getLong("expiration");

            IPermissionUser permissionUser = permissionProvider.getPermissionUser(uuid);
            IPermissionGroup permissionGroup = permissionProvider.getPermissionGroup(groupName);
            if (add) {
                if (permissionUser.getPermissionGroups().contains(permissionGroup)) return;

                permissionUser.addPermissionGroup(permissionGroup, new ExpirationData(permissionGroup, expiration));
                permissionUser.calculatePermissions();

                updateForumRank(uuid, permissionUser.getHighestGroup());
                return;
            }
            if (!permissionUser.getPermissionGroups().contains(permissionGroup)) return;

            permissionUser.removePermissionGroup(permissionGroup);
            permissionUser.calculatePermissions();
            updateForumRank(uuid, permissionUser.getHighestGroup());
        }
        if (event.getChannel().equalsIgnoreCase("createPermissionUser")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            if (permissionProvider.getPermissionUser(uuid) != null) return;

            permissionProvider.createPermissionUser(uuid, false);
        }
        if (event.getChannel().equalsIgnoreCase("nameStorage")) {
            JsonDocument jsonDocument = event.getData();
            String name = jsonDocument.getString("name");
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            nameStorageProvider.updateNameResult(name, uuid, false);
        }
        if (event.getChannel().equalsIgnoreCase("sendNickDatas")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            UUID nickedSkinUUID = UUID.fromString(jsonDocument.getString("nickedSkinUUID"));
            String nickName = jsonDocument.getString("nickName");
            String playerName = jsonDocument.getString("playerName");

            nickProvider.prepareNicKData(uuid, playerName, nickName, nickedSkinUUID, false);
        }

        if (event.getChannel().equalsIgnoreCase("sendNickData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));
            String nickName = jsonDocument.getString("nickName");
            String playerName = jsonDocument.getString("playerName");
            UUID nickedSkinUUID = UUID.fromString(jsonDocument.getString("nickedSkinUUID"));

            nickProvider.prepareNicKData(uuid, playerName, nickName, nickedSkinUUID,false);
        }

        if (event.getChannel().equalsIgnoreCase("removeNickData")) {
            JsonDocument jsonDocument = event.getData();
            UUID uuid = UUID.fromString(jsonDocument.getString("uuid"));

            nickProvider.removeNickData(uuid, false);
        }

        if (event.getChannel().equalsIgnoreCase("sendNickname")) {
            JsonDocument jsonDocument = event.getData();
            String nickName = jsonDocument.getString("nickName");
            boolean removed = jsonDocument.getBoolean("removed");
            if (removed) {
                nickProvider.removeNickName(nickName, false);
                return;
            }
            nickProvider.addNickName(nickName, false);
        }
    }

    private void updateForumRank(UUID uuid, IPermissionGroup permissionGroup) {
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(uuid);
        String updateRangURLString = "https://ravenix.net/update-rank.php?username=" + nameResultByUUID.getName() + "&rangid=" + getForumGroupID(permissionGroup) + "&pw=cB8trH0mHIav";
        DownloadHelper.openURLConnection(updateRangURLString, new HashMap<>(), inputStream -> {
            try {
               inputStream.getResponseMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
            inputStream.disconnect();
        }, Throwable::printStackTrace);
    }

    private int getForumGroupID(IPermissionGroup permissionGroup) {
        if (permissionGroup.getName().equalsIgnoreCase("Administrator")) {
            return 5;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator+")) {
            return 17;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Developer")) {
            return 6;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Moderator")) {
            return 8;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Senior-Content")) {
            return 16;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Developer")) {
            return 7;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Moderator")) {
            return 10;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Content")) {
            return 9;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Moderator")) {
            return 15;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Builder")) {
            return 11;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Designer")) {
            return 11;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Developer")) {
            return 19;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Junior-Content")) {
            return 18;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Creator")) {
            return 12;
        }
        if (permissionGroup.getName().equalsIgnoreCase("Premium")) {
            return 14;
        }
        return 3;
    }
}
