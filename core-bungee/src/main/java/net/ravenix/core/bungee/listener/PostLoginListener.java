package net.ravenix.core.bungee.listener;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.ip.IIPProvider;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.punishment.IPunishmentProvider;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class PostLoginListener
        implements Listener {

    @EventHandler
    public void playerJoin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();

        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();

        UUID uniqueId = player.getUniqueId();

        IPermissionUser permissionUser = permissionProvider.getPermissionUser(uniqueId);
        if (permissionUser == null) {
            permissionProvider.createPermissionUser(uniqueId, true);
            permissionUser = permissionProvider.getPermissionUser(uniqueId);
            BungeeCore.getInstance().getNameStorageProvider()
                    .updateNameResult(player.getName(), uniqueId, true);
        }
        BungeeCore.getInstance().getNameStorageProvider()
                .updateNameResult(player.getName(), uniqueId, false);
        permissionUser.calculatePermissions();

        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        String proxy = CloudNetDriver.getInstance().getComponentName();
        ProxyServer.getInstance().getScheduler().schedule(BungeeCore.getInstance(), () -> {
            if (player.getServer() != null) {
                corePlayerProvider.corePlayerConnect(uniqueId, player.getServer().getInfo().getName(), proxy, true);
            }
        }, 2, TimeUnit.SECONDS);
    }

    @EventHandler
    public void playerSwitchServer(ServerSwitchEvent event) {
        ProxiedPlayer player = event.getPlayer();
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();

        if (corePlayerProvider != null)
            corePlayerProvider.switchServer(player.getUniqueId(), event.getPlayer().getServer().getInfo().getName(), true);

    }
}
