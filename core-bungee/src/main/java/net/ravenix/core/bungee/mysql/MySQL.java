package net.ravenix.core.bungee.mysql;

import net.md_5.bungee.api.ProxyServer;
import net.ravenix.core.bungee.BungeeCore;

import java.sql.*;
import java.util.concurrent.TimeUnit;

public final class MySQL {

    public static Connection connection;

    public void connect() {
        try {
            connection = DriverManager
                    .getConnection("jdbc:mysql://116.202.58.73:3306/core?autoReconnect=true", "root", "FMzMbOxRzBJy");
            this.createTables();
            this.mySQLThread();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void mySQLThread() {
        ProxyServer.getInstance().getScheduler().schedule(BungeeCore.getInstance(), () -> {
            if (connection == null) {
                this.connect();
            } else {
                this.getResult("SELECT * FROM core_users");
            }
        }, 0L, 20, TimeUnit.MINUTES);
    }

    private void createTables() {
        this.update("CREATE TABLE IF NOT EXISTS chatfilter (word varchar(100), autoMute INT(2))");

        this.update("CREATE TABLE IF NOT EXISTS core_users (uuid varchar(100))");
        this.update("CREATE TABLE IF NOT EXISTS core_users_expirations (uuid varchar(100), groupName varchar(100), expiration varchar(100))");

        this.update("CREATE TABLE IF NOT EXISTS core_groups (name varchar(100), " +
                "color varchar(100), priority varchar(100), " +
                "defaultGroup varchar(100), tabPrefix varchar(100)," +
                "nameTagPrefix varchar(100), chatPrefix varchar(100), " +
                "displayPrefix varchar(100))");
        this.update("CREATE TABLE IF NOT EXISTS core_groups_permissions (groupName varchar(100), permission varchar(100), negate varchar(100))");
        this.update("CREATE TABLE IF NOT EXISTS core_groups_inheritances (groupName varchar(100), inherits varchar(100))");
        this.update("CREATE TABLE IF NOT EXISTS core_groups_primary (uuid varchar(100), groupName varchar(100))");

        this.update("CREATE TABLE IF NOT EXISTS nameStorage (name varchar(100), uuid varchar(100))");

        this.update("CREATE TABLE IF NOT EXISTS reportReason (name varchar(100))");

        this.update("CREATE TABLE IF NOT EXISTS skinData (uuid varchar(36), skinValue varchar(100), skinSignature varchar(100))");

        this.update("CREATE TABLE IF NOT EXISTS punishments (name varchar(100), punishType varchar(100), duration varchar(100), permission varchar(100))");
        this.update("CREATE TABLE IF NOT EXISTS punishmentLog (uuid varchar(100), executor varchar(100), timestamp TEXT, reason varchar(100), punishType varchar(100), points varchar(100), end TEXT)");
        this.update("CREATE TABLE IF NOT EXISTS punishmentData (uuid varchar(36), executor varchar(36), " +
                "reason varchar(100), punishType varchar(100), duration varchar(100))");
    }

    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public void update(String qry) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            disconnect();
            connect();
        }
    }

    public ResultSet getResult(String qry) {
        try {
            return connection.createStatement().executeQuery(qry);
        } catch (SQLException e) {

            e.printStackTrace();

            return null;
        }
    }
}



