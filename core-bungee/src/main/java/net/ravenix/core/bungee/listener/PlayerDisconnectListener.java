package net.ravenix.core.bungee.listener;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.nickname.INickProvider;
import net.ravenix.core.shared.player.ICorePlayerProvider;
import net.ravenix.core.shared.report.IReportProvider;

public final class PlayerDisconnectListener
        implements Listener {

    @EventHandler
    public void playerQuit(PlayerDisconnectEvent event) {
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        ProxiedPlayer proxiedPlayer = event.getPlayer();
        corePlayerProvider.corePlayerDisconnect(proxiedPlayer.getUniqueId(), true);

        IReportProvider reportProvider = BungeeCore.getInstance().getReportProvider();
        INickProvider nickProvider = BungeeCore.getInstance().getNickProvider();
        if (nickProvider.isNicked(proxiedPlayer.getUniqueId())) {
            nickProvider.unnickPlayer(proxiedPlayer.getUniqueId(), true);
        }
        if (reportProvider.isReported(proxiedPlayer.getUniqueId())) reportProvider.deleteReport(proxiedPlayer.getUniqueId(), true);
    }
}
