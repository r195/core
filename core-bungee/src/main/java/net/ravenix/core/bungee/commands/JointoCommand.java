package net.ravenix.core.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayerProvider;

public final class JointoCommand extends Command {

    public JointoCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.command.goto")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length != 1) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze§8: §b/goto <Name>");
            return;
        }
        String playerName = args[0];
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        NameResult nameResult = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(playerName);
        if (nameResult == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        ICorePlayerProvider corePlayerProvider = BungeeCore.getInstance().getCorePlayerProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(nameResult.getUuid());
        if (corePlayerProvider.getCorePlayer(nameResult.getUuid()) == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix()
                    + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §cist nicht online.");
            return;
        }
        if (proxiedPlayer.getServer().getInfo().getName().equalsIgnoreCase(corePlayerProvider.getCorePlayer(nameResult.getUuid()).getServer())) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu bist bereits auf dem selben Server wie "
                    + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + "§c.");
            return;
        }
        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(corePlayerProvider.getCorePlayer(nameResult.getUuid()).getServer());
        proxiedPlayer.connect(serverInfo);
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu bist "
                + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §ahintergesprungen.");
    }
}
