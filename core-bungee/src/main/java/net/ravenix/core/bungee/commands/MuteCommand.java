package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.name.NameResult;
import net.ravenix.core.shared.permission.PrefixType;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.player.ICorePlayer;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.punishment.Punishment;
import net.ravenix.core.shared.punishment.PunishmentType;
import net.ravenix.core.shared.report.IReportProvider;

import java.util.List;

public final class MuteCommand extends Command implements TabExecutor {
    public MuteCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (!proxiedPlayer.hasPermission("ravenix.command.mute")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung.");
            return;
        }
        if (args.length != 2) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "Nutze: §b/mute <Name> <Grund>");
            return;
        }
        String playerName = args[0];
        NameResult nameResult
                = BungeeCore.getInstance().getNameStorageProvider().getNameResultByName(playerName);
        if (nameResult == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler existiert nicht.");
            return;
        }
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        Punishment punishment
                = punishmentProvider.getPunishment(args[1]);
        if (punishment == null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Grund existiert nicht.");
            return;
        }
        if (!punishment.getPunishmentType().equals(PunishmentType.MUTE)) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Grund existiert nicht.");
            return;
        }
        if (!proxiedPlayer.hasPermission(punishment.getPermission())) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu kannst keinen Spieler für diesen Grund muten.");
            return;
        }
        if (proxiedPlayer.getUniqueId().equals(nameResult.getUuid())) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu kannst dich nicht selber muten.");
            return;
        }
        if (punishmentProvider.getMuteData(nameResult.getUuid()) != null) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDieser Spieler ist bereits gemuted.");
            return;
        }
        IPermissionUser permissionUser = BungeeCore.getInstance().getPermissionProvider().getPermissionUser(nameResult.getUuid());
        if (permissionUser.hasPermission("ravenix.team") && !proxiedPlayer.hasPermission("*")) {
            proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDu kannst diesen Spieler nicht muten.");
            return;
        }
        IReportProvider reportProvider = BungeeCore.getInstance().getReportProvider();
        if (reportProvider.isReported(nameResult.getUuid())) reportProvider.deleteReport(nameResult.getUuid(), true);
        proxiedPlayer.sendMessage(BungeeCore.getInstance().getPrefix() + "§aDu hast den Spieler " + permissionUser.getHighestGroup().getPrefix(PrefixType.DISPLAY) + nameResult.getName() + " §agemuted.");
        punishmentProvider.mutePlayer(nameResult.getUuid(), proxiedPlayer.getUniqueId(), punishment.getDisplay(), punishment.getPoints(), true);
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            List<String> tabComplete = Lists.newArrayList();
            if (args.length == 1) {
                List<ICorePlayer> allCorePlayers = BungeeCore.getInstance().getCorePlayerProvider().getAllCorePlayers();
                ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
                allCorePlayers.forEach(iCorePlayer -> {
                    if (iCorePlayer.getServer().equalsIgnoreCase(proxiedPlayer.getServer().getInfo().getName())) {
                        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
                        NameResult nameResultByUUID = nameStorageProvider.getNameResultByUUID(iCorePlayer.getUUID());
                        tabComplete.add(nameResultByUUID.getName());
                    }
                });
                return tabComplete;
            }
            if (args.length == 2) {
                List<Punishment> punishments = BungeeCore.getInstance().getPunishmentProvider().getPunishments();
                punishments.forEach(punishment -> {
                    if (commandSender.hasPermission(punishment.getPermission()) && punishment.getPunishmentType().equals(PunishmentType.MUTE)) {
                        tabComplete.add(punishment.getName());
                    }
                });
                return tabComplete;
            }
        }
        return Lists.newArrayList();
    }
}
