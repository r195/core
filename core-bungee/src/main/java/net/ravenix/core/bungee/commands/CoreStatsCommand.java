package net.ravenix.core.bungee.commands;

import com.google.common.collect.Lists;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.name.INameStorageProvider;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;
import net.ravenix.core.shared.punishment.IPunishmentProvider;
import net.ravenix.core.shared.util.SystemUUID;

import java.util.List;

public final class CoreStatsCommand extends Command {

    public CoreStatsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!commandSender.hasPermission("ravenix.command.corestats")) {
            commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§cDazu hast du keine Bereichtgung.");
            return;
        }
        INameStorageProvider nameStorageProvider = BungeeCore.getInstance().getNameStorageProvider();
        IPunishmentProvider punishmentProvider = BungeeCore.getInstance().getPunishmentProvider();
        int registerd_players = nameStorageProvider.getAllNameStorages().size();
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        List<IPermissionUser> permissionUsers = permissionProvider.getPermissionUsers();
        List<IPermissionUser> usersWithRank = Lists.newArrayList();
        List<IPermissionUser> teamMembers = Lists.newArrayList();

        int banned_players = punishmentProvider.getBanDatas().size();
        int muted_players = punishmentProvider.getMuteDatas().size();

        permissionUsers.forEach(permissionUser -> {
            if (permissionUser.hasPermission("ravenix.team")) {
                teamMembers.add(permissionUser);
            }
        });

        permissionUsers.forEach(permissionUser -> {
            if (!permissionUser.getHighestGroup().equals(permissionProvider.getDefaultGroup())) {
                if (!permissionUser.getUUID().equals(SystemUUID.getUUID())) {
                    usersWithRank.add(permissionUser);
                }
            }
        });

        int players_withTeamRank = teamMembers.size();
        int players_withRank = usersWithRank.size() - players_withTeamRank;

        commandSender.sendMessage("§3");
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§3Ravenix §7Server Statistiken§8:");
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Anzahl der registrieten Spieler§8: §b" + registerd_players);
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Anzahl der Spieler mit Rang§8: §b" + players_withRank);
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Anzahl der Teammitglieder§8: §b" + players_withTeamRank);
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Anzahl der gebannten Spieler§8: §b" + banned_players);
        commandSender.sendMessage(BungeeCore.getInstance().getPrefix() + "§7Anzahl der gemuteten Spieler§8: §b" + muted_players);
        commandSender.sendMessage("§3");
    }
}
