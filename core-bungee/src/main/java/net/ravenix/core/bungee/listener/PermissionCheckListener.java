package net.ravenix.core.bungee.listener;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.ravenix.core.bungee.BungeeCore;
import net.ravenix.core.shared.permission.IPermissionProvider;
import net.ravenix.core.shared.permission.user.IPermissionUser;

public final class PermissionCheckListener implements Listener {

    @EventHandler
    public void handlePermissionCheck(PermissionCheckEvent event) {
        if (!(event.getSender() instanceof ProxiedPlayer)) return;
        ProxiedPlayer player = (ProxiedPlayer) event.getSender();

        String permission = event.getPermission();
        if (permission == null || permission.isEmpty() || permission.equalsIgnoreCase("none")) {
            event.setHasPermission(true);
            return;
        }
        IPermissionProvider permissionProvider = BungeeCore.getInstance().getPermissionProvider();
        IPermissionUser permissionUser = permissionProvider.getPermissionUser(player.getUniqueId());
        if (permissionUser == null) {
            event.setHasPermission(false);
            return;
        }
        event.setHasPermission(permissionUser.hasPermission(permission));
    }
}
