package net.ravenix.core.shared.player;

import java.util.List;
import java.util.UUID;

public interface ICorePlayerProvider {

    ICorePlayer getCorePlayer(UUID uuid);

    List<ICorePlayer> getAllCorePlayers();

    void switchServer(UUID uuid, String server, boolean send);

    void corePlayerConnect(UUID uuid, String server, String proxy, boolean send);

    void corePlayerDisconnect(UUID uuid, boolean send);

    void sendOnlinePlayerData();

}
