package net.ravenix.core.shared.skin;

import java.util.UUID;

public interface ISkin {

    UUID getUUID();

    String getSkinValue();

    String getSkinSignature();

    void setSkinValue(String skinValue);

    void setSkinSignature(String skinSignature);

}
