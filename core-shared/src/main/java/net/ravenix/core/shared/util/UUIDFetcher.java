package net.ravenix.core.shared.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class UUIDFetcher {

    private final ExecutorService executor;

    public UUIDFetcher(int threads) {
        this.executor = Executors.newFixedThreadPool(threads);
    }

    public UUIDFetcher(ExecutorService executor) {
        this.executor = executor;
    }

    public UUID fetchUUID(String playerName) {
        try {
            // Get response from Mojang API
            URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + playerName);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() == 400) {
                System.err.println("There is no player with the name \"" + playerName + "\"!");
                return UUID.randomUUID();
            }

            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // Parse JSON response and get UUID
            JsonElement element = new JsonParser().parse(bufferedReader);
            JsonObject object = element.getAsJsonObject();
            String uuidAsString = object.get("id").getAsString();

            // Return UUID
            return parseUUIDFromString(uuidAsString);
        } catch (Exception exception) {
            return null;
        }
    }

    public void shutdown() {
        // Stop executor (This step is important!)
        executor.shutdown();
    }

    private UUID parseUUIDFromString(String uuidAsString) {
        String[] parts = {
                "0x" + uuidAsString.substring(0, 8),
                "0x" + uuidAsString.substring(8, 12),
                "0x" + uuidAsString.substring(12, 16),
                "0x" + uuidAsString.substring(16, 20),
                "0x" + uuidAsString.substring(20, 32)
        };

        long mostSigBits = Long.decode(parts[0]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[1]);
        mostSigBits <<= 16;
        mostSigBits |= Long.decode(parts[2]);

        long leastSigBits = Long.decode(parts[3]);
        leastSigBits <<= 48;
        leastSigBits |= Long.decode(parts[4]);

        return new UUID(mostSigBits, leastSigBits);
    }

}