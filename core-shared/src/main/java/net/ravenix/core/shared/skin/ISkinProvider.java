package net.ravenix.core.shared.skin;

import java.util.UUID;

public interface ISkinProvider {

    ISkin getSkin(UUID uuid);

    void updateSkin(UUID uuid, String skinValue, String skinSignature, boolean send, boolean insert);

    ISkin getRandomSkin();

}
