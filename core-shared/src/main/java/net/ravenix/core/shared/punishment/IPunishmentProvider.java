package net.ravenix.core.shared.punishment;

import java.util.List;
import java.util.UUID;

public interface IPunishmentProvider {

    PunishmentLog getSpecialPunishmentLog(UUID uuid, UUID executor, long end, long timestamp, String reason);

    List<BanData> getBanDatas();

    List<MuteData> getMuteDatas();

    Punishment getPunishment(String name);

    List<Punishment> getPunishments();

    BanData getBanData(UUID uuid);

    MuteData getMuteData(UUID uuid);

    void removePunishmentLog(UUID uuid, PunishmentLog punishmentLog, boolean send);

    void addPunishmentLog(UUID uuid, PunishmentLog punishmentLog);

    List<PunishmentLog> getPunishmentLog(UUID uuid);

    void banPlayer(UUID uuid, UUID executor, String reason, int points, boolean insert);

    void mutePlayer(UUID uuid, UUID executor, String reason, int points, boolean insert);

    void kickPlayer(UUID uuid, UUID executor, String reason, boolean send);

    void unbanPlayer(UUID uuid, UUID executor, boolean delete);

    void unmutePlayer(UUID uuid, UUID executor, boolean delete);

    boolean checkForBannedAccount(UUID uuid);

    boolean checkForMutedAccount(UUID uuid);

}
