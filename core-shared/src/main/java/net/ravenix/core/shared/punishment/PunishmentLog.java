package net.ravenix.core.shared.punishment;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class PunishmentLog {

    private final UUID uuid;
    private final UUID executor;
    private final long timestamp;
    private final String reason;
    private final PunishmentType punishmentType;
    private final int points;
    private final long end;

}
