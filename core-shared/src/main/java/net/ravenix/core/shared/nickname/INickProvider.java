package net.ravenix.core.shared.nickname;

import java.util.List;
import java.util.UUID;

public interface INickProvider {

    NickData getNickData(UUID uuid);

    void nickPlayer(UUID uuid, boolean send);

    void unnickPlayer(UUID uuid, boolean send);

    void prepareNicKData(UUID uuid, String playerName, String nickName, UUID nickedSkinUUID, boolean send);

    void removeNickData(UUID uuid, boolean send);

    List<String> getNickNames();

    String getRandomNickName();

    void addNickName(String nickName, boolean send);

    void removeNickName(String nickName, boolean send);

    boolean existsNickName(String nickName);

    boolean isNicked(UUID uuid);

    void sendNickDatas();

    List<String> getUsedNickNames();

}
