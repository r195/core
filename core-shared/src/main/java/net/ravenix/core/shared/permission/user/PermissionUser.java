package net.ravenix.core.shared.permission.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import net.ravenix.core.shared.permission.ExpirationData;
import net.ravenix.core.shared.permission.Permission;
import net.ravenix.core.shared.permission.group.IPermissionGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@NoArgsConstructor
public class PermissionUser implements IPermissionUser {

    private final List<IPermissionGroup> permissionGroups = new ArrayList<>();
    @Getter
    private final List<ExpirationData> groupExpirations = new ArrayList<>();
    private final List<Permission> clientPermissions = new ArrayList<>();

    private final List<Permission> calculatedPermissions = new ArrayList<>();

    private UUID uuid;

    public PermissionUser(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public UUID getUUID() {
        return this.uuid;
    }

    @Override
    public List<IPermissionGroup> getPermissionGroups() {
        return this.permissionGroups;
    }

    @Override
    public void addPermissionGroup(IPermissionGroup permissionGroup, ExpirationData expirationData) {
        if (this.permissionGroups.stream().anyMatch(group -> group.getName().equals(permissionGroup.getName()))) return;
        this.groupExpirations.add(expirationData);
        this.permissionGroups.add(permissionGroup);
    }

    @Override
    public void removePermissionGroup(IPermissionGroup permissionGroup) {
        ExpirationData expiration = getExpiration(permissionGroup);
        this.groupExpirations.remove(expiration);
        this.permissionGroups.removeIf(group -> group.getName().equals(permissionGroup.getName()));
    }

    @Override
    public ExpirationData getExpiration(String name) {
        return this.groupExpirations.stream().filter(expirationData -> {
            return expirationData.getPermissionGroup().getName().equalsIgnoreCase(name);
        }).findFirst().orElse(null);
    }

    @Override
    public List<String> getCalculatedPermissions() {
        return this.calculatedPermissions.stream()
                .filter(permission -> !permission.isNegate())
                .map(Permission::getPermission).collect(Collectors.toList());
    }

    @Override
    public List<Permission> getClientPermissions() {
        return this.clientPermissions;
    }

    @Override
    public void calculatePermissions() {
        this.calculatedPermissions.clear();

        this.permissionGroups.forEach(permissionGroup -> {
            if (permissionGroup.calculateGroupInheritances()) {
                permissionGroup.getInheritances().forEach(inheritance -> {
                    if (inheritance != null) {
                        inheritance.getPermissions().forEach(permission -> {
                            if (!this.calculatedPermissions.contains(permission))
                                this.calculatedPermissions.add(permission);
                        });
                    }
                });
            }
            permissionGroup.getPermissions().forEach(permission -> {
                if (!this.calculatedPermissions.contains(permission))
                    this.calculatedPermissions.add(permission);
            });
        });
    }

    @Override
    public boolean hasPermission(String permission) {
        if (this.calculatedPermissions.isEmpty())
            this.calculatePermissions();

        boolean hasPermission = false;

        for (Permission calculatedPermission : this.calculatedPermissions) {
            if (calculatedPermission.getPermission().equalsIgnoreCase("*"))
                hasPermission = true;

            if (calculatedPermission.getPermission().equalsIgnoreCase("*") && calculatedPermission.isNegate())
                hasPermission = false;

            if (calculatedPermission.getPermission().equalsIgnoreCase(permission))
                hasPermission = true;

            if (calculatedPermission.getPermission().equalsIgnoreCase(permission) && calculatedPermission.isNegate())
                hasPermission = false;
        }

        return hasPermission;
    }

    @Override
    public List<ExpirationData> getExpirations() {
        return this.groupExpirations;
    }
}

