package net.ravenix.core.shared.report;

import java.util.List;
import java.util.UUID;

public interface IReportProvider {

    ReportReason getReportCause(String name);

    IReport getReport(UUID suspect);

    boolean isReported(UUID suspect);

    void reportPlayer(UUID suspect, UUID reporter, String reason, String server, UUID spectator, boolean send);

    void deleteReport(UUID suspect, boolean send);

    List<IReport> getReports();

    List<IReport> getOpenReports();

    void sendOpenReportData();

}
