package net.ravenix.core.shared.report;

import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class Report implements IReport {

    private final UUID suspect;
    private final UUID reporter;
    private final String reason;
    private final String server;
    private UUID spectator;

    @Override
    public UUID getSuspect() {
        return this.suspect;
    }

    @Override
    public UUID getReporter() {
        return this.reporter;
    }

    @Override
    public String getReason() {
        return this.reason;
    }

    @Override
    public String getServer() {
        return this.server;
    }

    @Override
    public UUID getSpectator() {
        return spectator;
    }

    @Override
    public void setSpectator(UUID uuid) {
        this.spectator = uuid;
    }
}
