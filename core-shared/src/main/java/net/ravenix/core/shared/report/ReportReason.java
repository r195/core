package net.ravenix.core.shared.report;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ReportReason {

    private final String name;

}
