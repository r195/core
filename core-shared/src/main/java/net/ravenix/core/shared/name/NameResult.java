package net.ravenix.core.shared.name;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NameResult {

    private String name;
    private UUID uuid;

}
