package net.ravenix.core.shared.permission;

import net.ravenix.core.shared.permission.group.IPermissionGroup;
import net.ravenix.core.shared.permission.user.IPermissionUser;

import java.util.List;
import java.util.UUID;

public interface IPermissionProvider {

    IPermissionGroup getPermissionGroup(String groupName);

    IPermissionUser getPermissionUser(UUID uuid);

    List<IPermissionGroup> getPermissionGroups();

    void addPermissionGroup(IPermissionUser permissionUser, IPermissionGroup permissionGroup, UUID executor, long duration, boolean insert);

    default void addPermissionGroup(IPermissionUser permissionUser, UUID executor, IPermissionGroup permissionGroup, boolean insert) {
        this.addPermissionGroup(permissionUser, permissionGroup, executor, -1, insert);
    }

    void removePermissionGroup(IPermissionUser permissionUser, UUID executor, IPermissionGroup permissionGroup);

    IPermissionGroup getDefaultGroup();

    void createPermissionUser(UUID uuid, boolean insert);

    List<IPermissionUser> getPermissionUsers();

}
