package net.ravenix.core.shared.punishment;

import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class MuteData {

    private final UUID uuid;
    private final UUID executor;
    private final long duration;
    private final String reason;
    private final PunishmentType punishmentType;

    public UUID getUUID() {
        return uuid;
    }

    public UUID getExecutor() {
        return executor;
    }

    public String getReason() {
        return reason;
    }

    public long getDuration() {
        return duration;
    }

    public PunishmentType getPunishmentType() {
        return punishmentType;
    }
}
