package net.ravenix.core.shared.tablist;

public interface IInstanceCreator<K, V> {

    V create(K k);

}
