package net.ravenix.core.shared.punishment;

public enum PunishmentType {

    BAN,
    MUTE,
    KICK;

}
