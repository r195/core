package net.ravenix.core.shared.permission;

public enum PrefixType {

    CHAT,
    NAMETAG,
    DISPLAY,
    TAB

}