package net.ravenix.core.shared.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public final class DownloadHelper {
    private DownloadHelper() {
        throw new UnsupportedOperationException();
    }

    public static void openConnection(String url, Consumer<InputStream> consumer) {
        openConnection(url, new HashMap<>(), consumer);
    }

    public static void openConnection(String url, Map<String, String> headers, Consumer<InputStream> inputStreamConsumer) {
        openConnection(url, headers, inputStreamConsumer, Throwable::printStackTrace);
    }

    public static void openConnection(String url, Map<String, String> headers, Consumer<InputStream> inputStreamConsumer, Consumer<Throwable> exceptionConsumer) {
        openURLConnection(url, headers, httpURLConnection -> {
            try (InputStream stream = httpURLConnection.getInputStream()) {
                inputStreamConsumer.accept(stream);
            } catch (IOException ex) {
                exceptionConsumer.accept(ex);
            }
        }, exceptionConsumer);
    }

    public static void openURLConnection(String url, Map<String, String> headers, Consumer<HttpURLConnection> connectionConsumer, Consumer<Throwable> exceptionConsumer) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection)(new URL(url)).openConnection();
            httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            headers.forEach(httpURLConnection::setRequestProperty);
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.connect();
            connectionConsumer.accept(httpURLConnection);
            httpURLConnection.disconnect();
        } catch (Exception throwable) {
            exceptionConsumer.accept(throwable);
        }
    }

    private static String getSize(long size) {
        if (size >= 1048576L)
            return Math.round((float)size / 1048576.0F) + "MB";
        if (size >= 1024L)
            return Math.round((float)size / 1024.0F) + "KB";
        return Math.round((float)size) + "B";
    }
}

