package net.ravenix.core.shared.chatfilter;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class Word {

    private final String word;
    private final boolean autoMute;

}
