package net.ravenix.core.shared.permission.group;

import lombok.Getter;
import lombok.NoArgsConstructor;
import net.ravenix.core.shared.permission.Permission;
import net.ravenix.core.shared.permission.PrefixType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@NoArgsConstructor
public class PermissionGroup implements IPermissionGroup {

    private final Map<PrefixType, String> prefixes = new HashMap<>();
    private final List<Permission> permissions = new ArrayList<>();
    private final List<IPermissionGroup> inheritances = new CopyOnWriteArrayList<>();

    private String name;
    private int priority;
    private boolean defaultGroup;
    private String color;

    public PermissionGroup(String name, int priority, boolean defaultGroup, String color) {
        this.name = name;
        this.priority = priority;
        this.defaultGroup = defaultGroup;
        this.color = color;
    }

    @Override
    public boolean hasPermission(String permission) {
        List<Permission> permissions = calculatePermissions();
        return permissions.stream().anyMatch(permission1 ->
                permission1.getPermission().equalsIgnoreCase(permission) && !permission1.isNegate()
        );
    }

    @Override
    public boolean isDefault() {
        return this.defaultGroup;
    }

    @Override
    public String getPrefix(PrefixType prefixType) {
        return this.prefixes.get(prefixType);
    }

    private List<Permission> calculatePermissions() {
        List<Permission> permissions = new ArrayList<>(this.permissions);

        this.inheritances.forEach(permissionGroup -> {
            PermissionGroup defaultPermissionGroup = (PermissionGroup) permissionGroup;
            this.permissions.addAll(defaultPermissionGroup.calculatePermissions());
        });
        return permissions;
    }

    public void addPermissions(List<Permission> permissions) {
        this.permissions.addAll(permissions);
    }

    @Override
    public boolean calculateGroupInheritances() {
        if (this.inheritances.isEmpty()) return true;
        for (IPermissionGroup permissionGroup : this.inheritances) {
            if (permissionGroup.calculateGroupInheritances()) {
                for (IPermissionGroup inheritance : permissionGroup.getInheritances()) {
                    if (this.inheritances.stream().noneMatch(permissionGroup1 -> permissionGroup1.getName().equals(inheritance.getName())))
                        this.inheritances.add(inheritance);
                }
            }
            return true;
        }
        calculatePermissions();
        return false;
    }

    @Override
    public void addInheritance(IPermissionGroup permissionGroup) {
        this.inheritances.add(permissionGroup);
    }

    public void setPrefixes(Map<PrefixType, String> prefixes) {
        this.prefixes.clear();
        this.prefixes.putAll(prefixes);
    }
}
