package net.ravenix.core.shared.name;

import java.util.List;
import java.util.UUID;

public interface INameStorageProvider {

    List<NameResult> getAllNameStorages();

    NameResult getNameResultByName(String playerName);

    NameResult getNameResultByUUID(UUID uuid);

    void updateNameResult(String playerName, UUID uuid, boolean insert);

}
