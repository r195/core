package net.ravenix.core.shared.player;

import java.util.UUID;

public interface ICorePlayer {

    UUID getUUID();

    void setOnline(boolean state);

    boolean isOnline();

    void setServer(String serverName);

    String getServer();

    void setProxy(String proxyName);

    String getProxy();

}
