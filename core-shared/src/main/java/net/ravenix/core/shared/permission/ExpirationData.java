package net.ravenix.core.shared.permission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.ravenix.core.shared.permission.group.IPermissionGroup;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExpirationData {

    private IPermissionGroup permissionGroup;
    private long expiration;

}
