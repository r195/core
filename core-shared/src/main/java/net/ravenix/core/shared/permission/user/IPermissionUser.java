package net.ravenix.core.shared.permission.user;

import net.ravenix.core.shared.permission.ExpirationData;
import net.ravenix.core.shared.permission.Permission;
import net.ravenix.core.shared.permission.group.IPermissionGroup;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public interface IPermissionUser {

    UUID getUUID();

    List<IPermissionGroup> getPermissionGroups();

    default IPermissionGroup getHighestGroup() {
        return getPermissionGroups().stream().min(Comparator.comparingInt(IPermissionGroup::getPriority)).orElse(null);
    }

    default IPermissionGroup getLowestGroup() {
        return getPermissionGroups().stream().max(Comparator.comparingInt(IPermissionGroup::getPriority)).orElse(null);
    }

    void addPermissionGroup(IPermissionGroup permissionGroup, ExpirationData expirationData);

    void removePermissionGroup(IPermissionGroup permissionGroup);

    boolean hasPermission(String permission);

    List<Permission> getClientPermissions();

    void calculatePermissions();

    default void addClientPermission(Permission permission) {
        this.addClientPermission(permission.getPermission(), permission.isNegate());
    }

    default void addClientPermission(String permission, boolean negate) {
        this.getClientPermissions().add(new Permission(permission, negate));
    }

    ExpirationData getExpiration(String name);

    default ExpirationData getExpiration(IPermissionGroup permissionGroup) {
        return getExpiration(permissionGroup.getName());
    }

    List<String> getCalculatedPermissions();

    List<ExpirationData> getExpirations();


}
