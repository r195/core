package net.ravenix.core.shared.report;

import java.util.UUID;

public interface IReport {

    UUID getSuspect();

    UUID getReporter();

    String getReason();

    String getServer();

    UUID getSpectator();

    void setSpectator(UUID uuid);

}
