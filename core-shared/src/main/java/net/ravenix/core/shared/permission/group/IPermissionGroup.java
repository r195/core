package net.ravenix.core.shared.permission.group;

import net.ravenix.core.shared.permission.Permission;
import net.ravenix.core.shared.permission.PrefixType;

import java.util.List;

public interface IPermissionGroup {

    String getName();

    String getColor();

    default String getColoredName() {
        return getColor() + getName();
    }

    List<Permission> getPermissions();

    boolean hasPermission(String permission);

    List<IPermissionGroup> getInheritances();

    boolean calculateGroupInheritances();

    void addInheritance(IPermissionGroup permissionGroup);

    int getPriority();

    boolean isDefault();

    String getPrefix(PrefixType prefixType);

    default void addPermission(Permission permission) {
        this.getPermissions().add(permission);
    }

    default void addPermission(String permission) {
        this.getPermissions().add(new Permission(permission, false));
    }

    default void addPermission(String permission, boolean negate) {
        this.getPermissions().add(new Permission(permission, negate));
    }

}
