package net.ravenix.core.shared.util;

import java.util.UUID;

public class SystemUUID {

    public static UUID getUUID() {
        return UUID.fromString("00000000-0000-0000-0000-000000000000");
    }
}
