package net.ravenix.core.shared.ip;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface IIPProvider {

    Map<UUID, String> getAllIPs();

    String getIPAddress(UUID uuid);

    List<UUID> getAllAccountsFromIP(String ipAddress, UUID uuid);

    void createIPAddress(UUID uuid, String ipAddress, boolean send);

    void applyIPAdress(UUID uuid, String ipAddress, boolean send);

}
