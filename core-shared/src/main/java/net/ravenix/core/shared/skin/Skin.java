package net.ravenix.core.shared.skin;

import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class Skin implements ISkin {

    private final UUID uuid;
    private String skinValue;
    private String skinSignature;

    @Override
    public UUID getUUID() {
        return this.uuid;
    }

    @Override
    public String getSkinValue() {
        return this.skinValue;
    }

    @Override
    public String getSkinSignature() {
        return this.skinSignature;
    }

    @Override
    public void setSkinValue(String skinValue) {
        this.skinValue = skinValue;
    }

    @Override
    public void setSkinSignature(String skinSignature) {
        this.skinSignature = skinSignature;
    }
}
