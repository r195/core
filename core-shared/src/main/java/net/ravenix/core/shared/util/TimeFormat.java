package net.ravenix.core.shared.util;

import java.text.SimpleDateFormat;

public class TimeFormat {

    public static String format(long millis) {
        if (millis == -1) {
            return "§4PERMANENT";
        }
        return new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss").format(millis);
    }

}
