package net.ravenix.core.shared.punishment;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Punishment {

    private final String name;
    private final String display;
    private final PunishmentType punishmentType;
    private final int points;
    private final String permission;

}
