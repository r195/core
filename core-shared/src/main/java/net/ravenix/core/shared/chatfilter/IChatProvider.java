package net.ravenix.core.shared.chatfilter;

import java.util.List;
import java.util.UUID;

public interface IChatProvider {

    List<Word> getForbiddenWords();

    boolean isForbidden(String message);

    Word getForbiddenWord(String message);

    void addForbiddenWord(String string, boolean autoMute);

    void handleVerbose(UUID uuid, String writtenMessage, String server, boolean send);
}
