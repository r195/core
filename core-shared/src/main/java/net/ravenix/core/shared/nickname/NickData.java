package net.ravenix.core.shared.nickname;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.ravenix.core.shared.skin.ISkin;

import java.util.UUID;

@Getter
@AllArgsConstructor
public final class NickData {

    private final String playerName;
    private final String nickName;
    private final UUID nickedUUID;
    private final ISkin nickedSkin;

}
